#-- start of make_header -----------------

#====================================
#  Document DCHValidationComponentsList
#
#   Generated Wed Oct  4 15:28:39 2017  by sarnling
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_DCHValidationComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_DCHValidationComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_DCHValidationComponentsList

DCHValidation_tag = $(tag)

#cmt_local_tagfile_DCHValidationComponentsList = $(DCHValidation_tag)_DCHValidationComponentsList.make
cmt_local_tagfile_DCHValidationComponentsList = $(bin)$(DCHValidation_tag)_DCHValidationComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

DCHValidation_tag = $(tag)

#cmt_local_tagfile_DCHValidationComponentsList = $(DCHValidation_tag).make
cmt_local_tagfile_DCHValidationComponentsList = $(bin)$(DCHValidation_tag).make

endif

include $(cmt_local_tagfile_DCHValidationComponentsList)
#-include $(cmt_local_tagfile_DCHValidationComponentsList)

ifdef cmt_DCHValidationComponentsList_has_target_tag

cmt_final_setup_DCHValidationComponentsList = $(bin)setup_DCHValidationComponentsList.make
cmt_dependencies_in_DCHValidationComponentsList = $(bin)dependencies_DCHValidationComponentsList.in
#cmt_final_setup_DCHValidationComponentsList = $(bin)DCHValidation_DCHValidationComponentsListsetup.make
cmt_local_DCHValidationComponentsList_makefile = $(bin)DCHValidationComponentsList.make

else

cmt_final_setup_DCHValidationComponentsList = $(bin)setup.make
cmt_dependencies_in_DCHValidationComponentsList = $(bin)dependencies.in
#cmt_final_setup_DCHValidationComponentsList = $(bin)DCHValidationsetup.make
cmt_local_DCHValidationComponentsList_makefile = $(bin)DCHValidationComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)DCHValidationsetup.make

#DCHValidationComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'DCHValidationComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = DCHValidationComponentsList/
#DCHValidationComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
##
componentslistfile = DCHValidation.components
COMPONENTSLIST_DIR = ../$(tag)
fulllibname = libDCHValidation.$(shlibsuffix)

DCHValidationComponentsList :: ${COMPONENTSLIST_DIR}/$(componentslistfile)
	@:

${COMPONENTSLIST_DIR}/$(componentslistfile) :: $(bin)$(fulllibname)
	@echo 'Generating componentslist file for $(fulllibname)'
	cd ../$(tag);$(listcomponents_cmd) --output ${COMPONENTSLIST_DIR}/$(componentslistfile) $(fulllibname)

install :: DCHValidationComponentsListinstall
DCHValidationComponentsListinstall :: DCHValidationComponentsList

uninstall :: DCHValidationComponentsListuninstall
DCHValidationComponentsListuninstall :: DCHValidationComponentsListclean

DCHValidationComponentsListclean ::
	@echo 'Deleting $(componentslistfile)'
	@rm -f ${COMPONENTSLIST_DIR}/$(componentslistfile)

#-- start of cleanup_header --------------

clean :: DCHValidationComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(DCHValidationComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

DCHValidationComponentsListclean ::
#-- end of cleanup_header ---------------

#-- start of make_header -----------------

#====================================
#  Library DCHValidation
#
#   Generated Wed Oct  4 15:28:19 2017  by sarnling
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_DCHValidation_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_DCHValidation_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_DCHValidation

DCHValidation_tag = $(tag)

#cmt_local_tagfile_DCHValidation = $(DCHValidation_tag)_DCHValidation.make
cmt_local_tagfile_DCHValidation = $(bin)$(DCHValidation_tag)_DCHValidation.make

else

tags      = $(tag),$(CMTEXTRATAGS)

DCHValidation_tag = $(tag)

#cmt_local_tagfile_DCHValidation = $(DCHValidation_tag).make
cmt_local_tagfile_DCHValidation = $(bin)$(DCHValidation_tag).make

endif

include $(cmt_local_tagfile_DCHValidation)
#-include $(cmt_local_tagfile_DCHValidation)

ifdef cmt_DCHValidation_has_target_tag

cmt_final_setup_DCHValidation = $(bin)setup_DCHValidation.make
cmt_dependencies_in_DCHValidation = $(bin)dependencies_DCHValidation.in
#cmt_final_setup_DCHValidation = $(bin)DCHValidation_DCHValidationsetup.make
cmt_local_DCHValidation_makefile = $(bin)DCHValidation.make

else

cmt_final_setup_DCHValidation = $(bin)setup.make
cmt_dependencies_in_DCHValidation = $(bin)dependencies.in
#cmt_final_setup_DCHValidation = $(bin)DCHValidationsetup.make
cmt_local_DCHValidation_makefile = $(bin)DCHValidation.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)DCHValidationsetup.make

#DCHValidation :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'DCHValidation'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = DCHValidation/
#DCHValidation::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

DCHValidationlibname   = $(bin)$(library_prefix)DCHValidation$(library_suffix)
DCHValidationlib       = $(DCHValidationlibname).a
DCHValidationstamp     = $(bin)DCHValidation.stamp
DCHValidationshstamp   = $(bin)DCHValidation.shstamp

DCHValidation :: dirs  DCHValidationLIB
	$(echo) "DCHValidation ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#DCHValidationLIB :: $(DCHValidationlib) $(DCHValidationshstamp)
DCHValidationLIB :: $(DCHValidationshstamp)
	$(echo) "DCHValidation : library ok"

$(DCHValidationlib) :: $(bin)DCHValidationAlg.o $(bin)DCHValidation_entries.o $(bin)DCHValidation_load.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(DCHValidationlib) $?
	$(lib_silent) $(ranlib) $(DCHValidationlib)
	$(lib_silent) cat /dev/null >$(DCHValidationstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(DCHValidationlibname).$(shlibsuffix) :: $(bin)DCHValidationAlg.o $(bin)DCHValidation_entries.o $(bin)DCHValidation_load.o $(use_requirements) $(DCHValidationstamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)DCHValidationAlg.o $(bin)DCHValidation_entries.o $(bin)DCHValidation_load.o $(DCHValidation_shlibflags)
	$(lib_silent) cat /dev/null >$(DCHValidationstamp) && \
	  cat /dev/null >$(DCHValidationshstamp)

$(DCHValidationshstamp) :: $(DCHValidationlibname).$(shlibsuffix)
	$(lib_silent) if test -f $(DCHValidationlibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(DCHValidationstamp) && \
	  cat /dev/null >$(DCHValidationshstamp) ; fi

DCHValidationclean ::
	$(cleanup_echo) objects DCHValidation
	$(cleanup_silent) /bin/rm -f $(bin)DCHValidationAlg.o $(bin)DCHValidation_entries.o $(bin)DCHValidation_load.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)DCHValidationAlg.o $(bin)DCHValidation_entries.o $(bin)DCHValidation_load.o) $(patsubst %.o,%.dep,$(bin)DCHValidationAlg.o $(bin)DCHValidation_entries.o $(bin)DCHValidation_load.o) $(patsubst %.o,%.d.stamp,$(bin)DCHValidationAlg.o $(bin)DCHValidation_entries.o $(bin)DCHValidation_load.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf DCHValidation_deps DCHValidation_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
DCHValidationinstallname = $(library_prefix)DCHValidation$(library_suffix).$(shlibsuffix)

DCHValidation :: DCHValidationinstall ;

install :: DCHValidationinstall ;

DCHValidationinstall :: $(install_dir)/$(DCHValidationinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(DCHValidationinstallname) :: $(bin)$(DCHValidationinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(DCHValidationinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##DCHValidationclean :: DCHValidationuninstall

uninstall :: DCHValidationuninstall ;

DCHValidationuninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(DCHValidationinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),DCHValidationclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)DCHValidationAlg.d

$(bin)$(binobj)DCHValidationAlg.d :

$(bin)$(binobj)DCHValidationAlg.o : $(cmt_final_setup_DCHValidation)

$(bin)$(binobj)DCHValidationAlg.o : $(src)DCHValidationAlg.cxx
	$(cpp_echo) $(src)DCHValidationAlg.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(DCHValidation_pp_cppflags) $(lib_DCHValidation_pp_cppflags) $(DCHValidationAlg_pp_cppflags) $(use_cppflags) $(DCHValidation_cppflags) $(lib_DCHValidation_cppflags) $(DCHValidationAlg_cppflags) $(DCHValidationAlg_cxx_cppflags)  $(src)DCHValidationAlg.cxx
endif
endif

else
$(bin)DCHValidation_dependencies.make : $(DCHValidationAlg_cxx_dependencies)

$(bin)DCHValidation_dependencies.make : $(src)DCHValidationAlg.cxx

$(bin)$(binobj)DCHValidationAlg.o : $(DCHValidationAlg_cxx_dependencies)
	$(cpp_echo) $(src)DCHValidationAlg.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(DCHValidation_pp_cppflags) $(lib_DCHValidation_pp_cppflags) $(DCHValidationAlg_pp_cppflags) $(use_cppflags) $(DCHValidation_cppflags) $(lib_DCHValidation_cppflags) $(DCHValidationAlg_cppflags) $(DCHValidationAlg_cxx_cppflags)  $(src)DCHValidationAlg.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),DCHValidationclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)DCHValidation_entries.d

$(bin)$(binobj)DCHValidation_entries.d :

$(bin)$(binobj)DCHValidation_entries.o : $(cmt_final_setup_DCHValidation)

$(bin)$(binobj)DCHValidation_entries.o : $(src)components/DCHValidation_entries.cxx
	$(cpp_echo) $(src)components/DCHValidation_entries.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(DCHValidation_pp_cppflags) $(lib_DCHValidation_pp_cppflags) $(DCHValidation_entries_pp_cppflags) $(use_cppflags) $(DCHValidation_cppflags) $(lib_DCHValidation_cppflags) $(DCHValidation_entries_cppflags) $(DCHValidation_entries_cxx_cppflags) -I../src/components $(src)components/DCHValidation_entries.cxx
endif
endif

else
$(bin)DCHValidation_dependencies.make : $(DCHValidation_entries_cxx_dependencies)

$(bin)DCHValidation_dependencies.make : $(src)components/DCHValidation_entries.cxx

$(bin)$(binobj)DCHValidation_entries.o : $(DCHValidation_entries_cxx_dependencies)
	$(cpp_echo) $(src)components/DCHValidation_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(DCHValidation_pp_cppflags) $(lib_DCHValidation_pp_cppflags) $(DCHValidation_entries_pp_cppflags) $(use_cppflags) $(DCHValidation_cppflags) $(lib_DCHValidation_cppflags) $(DCHValidation_entries_cppflags) $(DCHValidation_entries_cxx_cppflags) -I../src/components $(src)components/DCHValidation_entries.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),DCHValidationclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)DCHValidation_load.d

$(bin)$(binobj)DCHValidation_load.d :

$(bin)$(binobj)DCHValidation_load.o : $(cmt_final_setup_DCHValidation)

$(bin)$(binobj)DCHValidation_load.o : $(src)components/DCHValidation_load.cxx
	$(cpp_echo) $(src)components/DCHValidation_load.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(DCHValidation_pp_cppflags) $(lib_DCHValidation_pp_cppflags) $(DCHValidation_load_pp_cppflags) $(use_cppflags) $(DCHValidation_cppflags) $(lib_DCHValidation_cppflags) $(DCHValidation_load_cppflags) $(DCHValidation_load_cxx_cppflags) -I../src/components $(src)components/DCHValidation_load.cxx
endif
endif

else
$(bin)DCHValidation_dependencies.make : $(DCHValidation_load_cxx_dependencies)

$(bin)DCHValidation_dependencies.make : $(src)components/DCHValidation_load.cxx

$(bin)$(binobj)DCHValidation_load.o : $(DCHValidation_load_cxx_dependencies)
	$(cpp_echo) $(src)components/DCHValidation_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(DCHValidation_pp_cppflags) $(lib_DCHValidation_pp_cppflags) $(DCHValidation_load_pp_cppflags) $(use_cppflags) $(DCHValidation_cppflags) $(lib_DCHValidation_cppflags) $(DCHValidation_load_cppflags) $(DCHValidation_load_cxx_cppflags) -I../src/components $(src)components/DCHValidation_load.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: DCHValidationclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(DCHValidation.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

DCHValidationclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library DCHValidation
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)DCHValidation$(library_suffix).a $(library_prefix)DCHValidation$(library_suffix).$(shlibsuffix) DCHValidation.stamp DCHValidation.shstamp
#-- end of cleanup_library ---------------

#-- start of make_header -----------------

#====================================
#  Document DCHValidationConfDbMerge
#
#   Generated Wed Oct  4 15:28:39 2017  by sarnling
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_DCHValidationConfDbMerge_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_DCHValidationConfDbMerge_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_DCHValidationConfDbMerge

DCHValidation_tag = $(tag)

#cmt_local_tagfile_DCHValidationConfDbMerge = $(DCHValidation_tag)_DCHValidationConfDbMerge.make
cmt_local_tagfile_DCHValidationConfDbMerge = $(bin)$(DCHValidation_tag)_DCHValidationConfDbMerge.make

else

tags      = $(tag),$(CMTEXTRATAGS)

DCHValidation_tag = $(tag)

#cmt_local_tagfile_DCHValidationConfDbMerge = $(DCHValidation_tag).make
cmt_local_tagfile_DCHValidationConfDbMerge = $(bin)$(DCHValidation_tag).make

endif

include $(cmt_local_tagfile_DCHValidationConfDbMerge)
#-include $(cmt_local_tagfile_DCHValidationConfDbMerge)

ifdef cmt_DCHValidationConfDbMerge_has_target_tag

cmt_final_setup_DCHValidationConfDbMerge = $(bin)setup_DCHValidationConfDbMerge.make
cmt_dependencies_in_DCHValidationConfDbMerge = $(bin)dependencies_DCHValidationConfDbMerge.in
#cmt_final_setup_DCHValidationConfDbMerge = $(bin)DCHValidation_DCHValidationConfDbMergesetup.make
cmt_local_DCHValidationConfDbMerge_makefile = $(bin)DCHValidationConfDbMerge.make

else

cmt_final_setup_DCHValidationConfDbMerge = $(bin)setup.make
cmt_dependencies_in_DCHValidationConfDbMerge = $(bin)dependencies.in
#cmt_final_setup_DCHValidationConfDbMerge = $(bin)DCHValidationsetup.make
cmt_local_DCHValidationConfDbMerge_makefile = $(bin)DCHValidationConfDbMerge.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)DCHValidationsetup.make

#DCHValidationConfDbMerge :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'DCHValidationConfDbMerge'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = DCHValidationConfDbMerge/
#DCHValidationConfDbMerge::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_genconfDb_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.confdb file into a single
# <project>.confdb file in the (lib) install area

.PHONY: DCHValidationConfDbMerge DCHValidationConfDbMergeclean

# default is already '#'
#genconfDb_comment_char := "'#'"

instdir      := ${CMTINSTALLAREA}/$(tag)
confDbRef    := /afs/cern.ch/user/s/sarnling/DCHSignalSample/DCHValidation/genConf/DCHValidation/DCHValidation.confdb
stampConfDb  := $(confDbRef).stamp
mergedConfDb := $(instdir)/lib/$(project).confdb

DCHValidationConfDbMerge :: $(stampConfDb) $(mergedConfDb)
	@:

.NOTPARALLEL : $(stampConfDb) $(mergedConfDb)

$(stampConfDb) $(mergedConfDb) :: $(confDbRef)
	@echo "Running merge_genconfDb  DCHValidationConfDbMerge"
	$(merge_genconfDb_cmd) \
          --do-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)

DCHValidationConfDbMergeclean ::
	$(cleanup_silent) $(merge_genconfDb_cmd) \
          --un-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)	;
	$(cleanup_silent) $(remove_command) $(stampConfDb)
libDCHValidation_so_dependencies = ../x86_64-slc6-gcc47-opt/libDCHValidation.so
#-- start of cleanup_header --------------

clean :: DCHValidationConfDbMergeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(DCHValidationConfDbMerge.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

DCHValidationConfDbMergeclean ::
#-- end of cleanup_header ---------------

#-- start of make_header -----------------

#====================================
#  Document DCHValidationMergeComponentsList
#
#   Generated Wed Oct  4 15:28:39 2017  by sarnling
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_DCHValidationMergeComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_DCHValidationMergeComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_DCHValidationMergeComponentsList

DCHValidation_tag = $(tag)

#cmt_local_tagfile_DCHValidationMergeComponentsList = $(DCHValidation_tag)_DCHValidationMergeComponentsList.make
cmt_local_tagfile_DCHValidationMergeComponentsList = $(bin)$(DCHValidation_tag)_DCHValidationMergeComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

DCHValidation_tag = $(tag)

#cmt_local_tagfile_DCHValidationMergeComponentsList = $(DCHValidation_tag).make
cmt_local_tagfile_DCHValidationMergeComponentsList = $(bin)$(DCHValidation_tag).make

endif

include $(cmt_local_tagfile_DCHValidationMergeComponentsList)
#-include $(cmt_local_tagfile_DCHValidationMergeComponentsList)

ifdef cmt_DCHValidationMergeComponentsList_has_target_tag

cmt_final_setup_DCHValidationMergeComponentsList = $(bin)setup_DCHValidationMergeComponentsList.make
cmt_dependencies_in_DCHValidationMergeComponentsList = $(bin)dependencies_DCHValidationMergeComponentsList.in
#cmt_final_setup_DCHValidationMergeComponentsList = $(bin)DCHValidation_DCHValidationMergeComponentsListsetup.make
cmt_local_DCHValidationMergeComponentsList_makefile = $(bin)DCHValidationMergeComponentsList.make

else

cmt_final_setup_DCHValidationMergeComponentsList = $(bin)setup.make
cmt_dependencies_in_DCHValidationMergeComponentsList = $(bin)dependencies.in
#cmt_final_setup_DCHValidationMergeComponentsList = $(bin)DCHValidationsetup.make
cmt_local_DCHValidationMergeComponentsList_makefile = $(bin)DCHValidationMergeComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)DCHValidationsetup.make

#DCHValidationMergeComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'DCHValidationMergeComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = DCHValidationMergeComponentsList/
#DCHValidationMergeComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_componentslist_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.components file into a single
# <project>.components file in the (lib) install area
# If no InstallArea is present the fragment is dummy


.PHONY: DCHValidationMergeComponentsList DCHValidationMergeComponentsListclean

# default is already '#'
#genmap_comment_char := "'#'"

componentsListRef    := ../$(tag)/DCHValidation.components

ifdef CMTINSTALLAREA
componentsListDir    := ${CMTINSTALLAREA}/$(tag)/lib
mergedComponentsList := $(componentsListDir)/$(project).components
stampComponentsList  := $(componentsListRef).stamp
else
componentsListDir    := ../$(tag)
mergedComponentsList :=
stampComponentsList  :=
endif

DCHValidationMergeComponentsList :: $(stampComponentsList) $(mergedComponentsList)
	@:

.NOTPARALLEL : $(stampComponentsList) $(mergedComponentsList)

$(stampComponentsList) $(mergedComponentsList) :: $(componentsListRef)
	@echo "Running merge_componentslist  DCHValidationMergeComponentsList"
	$(merge_componentslist_cmd) --do-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList)

DCHValidationMergeComponentsListclean ::
	$(cleanup_silent) $(merge_componentslist_cmd) --un-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList) ;
	$(cleanup_silent) $(remove_command) $(stampComponentsList)
libDCHValidation_so_dependencies = ../x86_64-slc6-gcc47-opt/libDCHValidation.so
#-- start of cleanup_header --------------

clean :: DCHValidationMergeComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(DCHValidationMergeComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

DCHValidationMergeComponentsListclean ::
#-- end of cleanup_header ---------------

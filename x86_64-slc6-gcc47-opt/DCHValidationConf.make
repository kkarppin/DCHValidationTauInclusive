#-- start of make_header -----------------

#====================================
#  Document DCHValidationConf
#
#   Generated Wed Oct  4 15:28:37 2017  by sarnling
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_DCHValidationConf_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_DCHValidationConf_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_DCHValidationConf

DCHValidation_tag = $(tag)

#cmt_local_tagfile_DCHValidationConf = $(DCHValidation_tag)_DCHValidationConf.make
cmt_local_tagfile_DCHValidationConf = $(bin)$(DCHValidation_tag)_DCHValidationConf.make

else

tags      = $(tag),$(CMTEXTRATAGS)

DCHValidation_tag = $(tag)

#cmt_local_tagfile_DCHValidationConf = $(DCHValidation_tag).make
cmt_local_tagfile_DCHValidationConf = $(bin)$(DCHValidation_tag).make

endif

include $(cmt_local_tagfile_DCHValidationConf)
#-include $(cmt_local_tagfile_DCHValidationConf)

ifdef cmt_DCHValidationConf_has_target_tag

cmt_final_setup_DCHValidationConf = $(bin)setup_DCHValidationConf.make
cmt_dependencies_in_DCHValidationConf = $(bin)dependencies_DCHValidationConf.in
#cmt_final_setup_DCHValidationConf = $(bin)DCHValidation_DCHValidationConfsetup.make
cmt_local_DCHValidationConf_makefile = $(bin)DCHValidationConf.make

else

cmt_final_setup_DCHValidationConf = $(bin)setup.make
cmt_dependencies_in_DCHValidationConf = $(bin)dependencies.in
#cmt_final_setup_DCHValidationConf = $(bin)DCHValidationsetup.make
cmt_local_DCHValidationConf_makefile = $(bin)DCHValidationConf.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)DCHValidationsetup.make

#DCHValidationConf :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'DCHValidationConf'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = DCHValidationConf/
#DCHValidationConf::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genconfig_header
# Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

# Use genconf.exe to create configurables python modules, then have the
# normal python install procedure take over.

.PHONY: DCHValidationConf DCHValidationConfclean

confpy  := DCHValidationConf.py
conflib := $(bin)$(library_prefix)DCHValidation.$(shlibsuffix)
confdb  := DCHValidation.confdb
instdir := $(CMTINSTALLAREA)$(shared_install_subdir)/python/$(package)
product := $(instdir)/$(confpy)
initpy  := $(instdir)/__init__.py

ifdef GENCONF_ECHO
genconf_silent =
else
genconf_silent = $(silent)
endif

DCHValidationConf :: DCHValidationConfinstall

install :: DCHValidationConfinstall

DCHValidationConfinstall : /afs/cern.ch/user/s/sarnling/DCHSignalSample/DCHValidation/genConf/DCHValidation/$(confpy)
	@echo "Installing /afs/cern.ch/user/s/sarnling/DCHSignalSample/DCHValidation/genConf/DCHValidation in /afs/cern.ch/user/s/sarnling/DCHSignalSample/InstallArea/python" ; \
	 $(install_command) --exclude="*.py?" --exclude="__init__.py" --exclude="*.confdb" /afs/cern.ch/user/s/sarnling/DCHSignalSample/DCHValidation/genConf/DCHValidation /afs/cern.ch/user/s/sarnling/DCHSignalSample/InstallArea/python ; \

/afs/cern.ch/user/s/sarnling/DCHSignalSample/DCHValidation/genConf/DCHValidation/$(confpy) : $(conflib) /afs/cern.ch/user/s/sarnling/DCHSignalSample/DCHValidation/genConf/DCHValidation
	$(genconf_silent) $(genconfig_cmd)   -o /afs/cern.ch/user/s/sarnling/DCHSignalSample/DCHValidation/genConf/DCHValidation -p $(package) \
	  --configurable-module=GaudiKernel.Proxy \
	  --configurable-default-name=Configurable.DefaultName \
	  --configurable-algorithm=ConfigurableAlgorithm \
	  --configurable-algtool=ConfigurableAlgTool \
	  --configurable-auditor=ConfigurableAuditor \
          --configurable-service=ConfigurableService \
	  -i ../$(tag)/$(library_prefix)DCHValidation.$(shlibsuffix)

/afs/cern.ch/user/s/sarnling/DCHSignalSample/DCHValidation/genConf/DCHValidation:
	@ if [ ! -d /afs/cern.ch/user/s/sarnling/DCHSignalSample/DCHValidation/genConf/DCHValidation ] ; then mkdir -p /afs/cern.ch/user/s/sarnling/DCHSignalSample/DCHValidation/genConf/DCHValidation ; fi ;

DCHValidationConfclean :: DCHValidationConfuninstall
	$(cleanup_silent) $(remove_command) /afs/cern.ch/user/s/sarnling/DCHSignalSample/DCHValidation/genConf/DCHValidation/$(confpy) /afs/cern.ch/user/s/sarnling/DCHSignalSample/DCHValidation/genConf/DCHValidation/$(confdb)

uninstall :: DCHValidationConfuninstall

DCHValidationConfuninstall ::
	@$(uninstall_command) /afs/cern.ch/user/s/sarnling/DCHSignalSample/InstallArea/python
libDCHValidation_so_dependencies = ../x86_64-slc6-gcc47-opt/libDCHValidation.so
#-- start of cleanup_header --------------

clean :: DCHValidationConfclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(DCHValidationConf.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

DCHValidationConfclean ::
#-- end of cleanup_header ---------------

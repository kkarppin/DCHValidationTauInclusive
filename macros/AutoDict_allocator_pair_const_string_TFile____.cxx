#include "map"
#include "string"
class TFile;
#ifdef __CINT__ 
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;
#pragma link C++ class allocator<pair<const string,TFile*> >+;
#pragma link C++ class allocator<pair<const string,TFile*> >::*+;
#endif

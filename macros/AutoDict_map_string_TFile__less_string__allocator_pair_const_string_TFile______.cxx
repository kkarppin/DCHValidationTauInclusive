#include "map"
#include "string"
#include "string"
class TFile;
#ifdef __CINT__ 
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;
#pragma link C++ class map<string,TFile*,less<string>,allocator<pair<const string,TFile*> > >+;
#pragma link C++ class map<string,TFile*,less<string>,allocator<pair<const string,TFile*> > >::*;
#pragma link C++ operators map<string,TFile*,less<string>,allocator<pair<const string,TFile*> > >::iterator;
#pragma link C++ operators map<string,TFile*,less<string>,allocator<pair<const string,TFile*> > >::const_iterator;
#pragma link C++ operators map<string,TFile*,less<string>,allocator<pair<const string,TFile*> > >::reverse_iterator;
#endif

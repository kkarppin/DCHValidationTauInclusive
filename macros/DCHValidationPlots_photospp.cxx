{
  TFile*  file  =  new  TFile("/afs/cern.ch/user/m/mmuskinj/MC15evgenProduction/19.2.X.Y-VAL/run/DCHValidation.histo.root");
  TFile*  file2  = new  TFile("/afs/cern.ch/user/m/mmuskinj/MC15evgenProduction/19.2.X.Y-VAL/run/DCHValidation.photospp.histo.root");

  /********* Get Pointers to the Histograms *********/
  TH1D*    DecayType  =  (TH1D*)  file->Get("DCHValidationHistograms/DecayType");
  TH1D*    LeptonPairTypeP  =  (TH1D*)  file->Get("DCHValidationHistograms/LeptonPairTypeP");
  TH1D*    LeptonPairTypeN  =  (TH1D*)  file->Get("DCHValidationHistograms/LeptonPairTypeN");
  TH1D*    InvariantMassP  =  (TH1D*)  file->Get("DCHValidationHistograms/InvariantMassP");
  TH1D*    InvariantMassN  =  (TH1D*)  file->Get("DCHValidationHistograms/InvariantMassN");
  TH1D*    LeadingPtP  =  (TH1D*)  file->Get("DCHValidationHistograms/LeadingPtP");
  TH1D*    SubLeadingPtP  =  (TH1D*)  file->Get("DCHValidationHistograms/SubLeadingPtP");
  TH1D*    LeadingEtaP  =  (TH1D*)  file->Get("DCHValidationHistograms/LeadingEtaP");
  TH1D*    SubLeadingEtaP  =  (TH1D*)  file->Get("DCHValidationHistograms/SubLeadingEtaP");
  TH1D*    LeadingPtN  =  (TH1D*)  file->Get("DCHValidationHistograms/LeadingPtN");
  TH1D*    SubLeadingPtN  =  (TH1D*)  file->Get("DCHValidationHistograms/SubLeadingPtN");
  TH1D*    LeadingEtaN  =  (TH1D*)  file->Get("DCHValidationHistograms/LeadingEtaN");
  TH1D*    SubLeadingEtaN  =  (TH1D*)  file->Get("DCHValidationHistograms/SubLeadingEtaN");
  //photospp
  TH1D*    DecayType2 =  (TH1D*)  file2->Get("DCHValidationHistograms/DecayType");
  TH1D*    LeptonPairTypeP2 =  (TH1D*)  file2->Get("DCHValidationHistograms/LeptonPairTypeP");
  TH1D*    LeptonPairTypeN2 =  (TH1D*)  file2->Get("DCHValidationHistograms/LeptonPairTypeN");
  TH1D*    InvariantMassP2 =  (TH1D*)  file2->Get("DCHValidationHistograms/InvariantMassP");
  TH1D*    InvariantMassN2 =  (TH1D*)  file2->Get("DCHValidationHistograms/InvariantMassN");
  TH1D*    LeadingPtP2 =  (TH1D*)  file2->Get("DCHValidationHistograms/LeadingPtP");
  TH1D*    SubLeadingPtP2 =  (TH1D*)  file2->Get("DCHValidationHistograms/SubLeadingPtP");
  TH1D*    LeadingEtaP2 =  (TH1D*)  file2->Get("DCHValidationHistograms/LeadingEtaP");
  TH1D*    SubLeadingEtaP2 =  (TH1D*)  file2->Get("DCHValidationHistograms/SubLeadingEtaP");
  TH1D*    LeadingPtN2 =  (TH1D*)  file2->Get("DCHValidationHistograms/LeadingPtN");
  TH1D*    SubLeadingPtN2 =  (TH1D*)  file2->Get("DCHValidationHistograms/SubLeadingPtN");
  TH1D*    LeadingEtaN2 =  (TH1D*)  file2->Get("DCHValidationHistograms/LeadingEtaN");
  TH1D*    SubLeadingEtaN2 =  (TH1D*)  file2->Get("DCHValidationHistograms/SubLeadingEtaN");

  /********* Hisogram Axis Titles *********/
  //Decay  Type
  DecayType->GetYaxis()->SetTitle("decays");

  //Positive  Lepton  Pair  Type
  LeptonPairTypeP->GetYaxis()->SetTitle("pairs");

  //Negative  Lepton  Pair  Type
  LeptonPairTypeN->GetYaxis()->SetTitle("pairs");

  //Positive  pair  invariant  mass
  InvariantMassP->GetXaxis()->SetTitle("invariant mass l^{+} l^{+} (GeV)");
  std::ostringstream ppm;
  ppm << "pairs/(" << InvariantMassP->GetBinWidth(0) << " GeV)";
  InvariantMassP->GetYaxis()->SetTitle(ppm.str().c_str());

  //Negative  pair  invariant  mass
  InvariantMassN->GetXaxis()->SetTitle("invariant mass l^{-} l^{-} (GeV)");
  std::ostringstream npm;
  npm << "pairs/(" << InvariantMassN->GetBinWidth(0) << " GeV)";
  InvariantMassN->GetYaxis()->SetTitle(npm.str().c_str());

  //Positive leading lepton pt
  LeadingPtP->GetXaxis()->SetTitle("transverse momentum (GeV)");
  std::ostringstream pllp;
  pllp << "leptons/(" << LeadingPtP->GetBinWidth(0) << " GeV)";
  LeadingPtP->GetYaxis()->SetTitle(pllp.str().c_str());

  //Negative leading lepton pt
  LeadingPtN->GetXaxis()->SetTitle("transverse momentum (GeV)");
  std::ostringstream nllp;
  nllp << "leptons/(" << LeadingPtN->GetBinWidth(0) << " GeV)";
  LeadingPtN->GetYaxis()->SetTitle(nllp.str().c_str());

  //Positive sub-leading lepton pt
  SubLeadingPtP->GetXaxis()->SetTitle("transverse momentum (GeV)");
  std::ostringstream psllp;
  psllp << "leptons/(" << SubLeadingPtP->GetBinWidth(0) << " GeV)";
  SubLeadingPtP->GetYaxis()->SetTitle(psllp.str().c_str());

  //Negative sub-leading lepton pt
  SubLeadingPtN->GetXaxis()->SetTitle("transverse momentum (GeV)");
  std::ostringstream nsllp;
  nsllp << "leptons/(" << SubLeadingPtN->GetBinWidth(0) << " GeV)";
  SubLeadingPtN->GetYaxis()->SetTitle(nsllp.str().c_str());

  //Positive leading lepton eta
  LeadingEtaP->GetXaxis()->SetTitle("pseudorapidity");
  std::ostringstream plle;
  plle << "leptons/(" << LeadingEtaP->GetBinWidth(0) << ")";
  LeadingEtaP->GetYaxis()->SetTitle(plle.str().c_str());

  //Negative leading lepton pt
  LeadingEtaN->GetXaxis()->SetTitle("pseudorapidity");
  std::ostringstream nlle;
  nlle << "leptons/(" << LeadingEtaN->GetBinWidth(0) << ")";
  LeadingEtaN->GetYaxis()->SetTitle(nlle.str().c_str());

  //Positive sub-leading lepton pt
  SubLeadingEtaP->GetXaxis()->SetTitle("pseudorapidity");
  std::ostringstream pslle;
  pslle << "leptons/(" << SubLeadingEtaP->GetBinWidth(0) << ")";
  SubLeadingEtaP->GetYaxis()->SetTitle(pslle.str().c_str());

  //Negative sub-leading lepton pt
  SubLeadingEtaN->GetXaxis()->SetTitle("pseudorapidity");
  std::ostringstream nslle;
  nslle << "leptons/(" << SubLeadingEtaN->GetBinWidth(0) << ")";
  SubLeadingEtaN->GetYaxis()->SetTitle(nslle.str().c_str());

  /********* Draw Canvases and Save to File *********/
  DecayType2->SetLineColor(kRed);
  LeptonPairTypeP2->SetLineColor(kRed);
  InvariantMassP2->SetLineColor(kRed);
  LeadingPtP2->SetLineColor(kRed);
  SubLeadingPtP2->SetLineColor(kRed);
  LeadingEtaP2->SetLineColor(kRed);
  SubLeadingEtaP2->SetLineColor(kRed);
  LeptonPairTypeN2->SetLineColor(kRed);
  InvariantMassN2->SetLineColor(kRed);
  LeadingPtN2->SetLineColor(kRed);
  SubLeadingPtN2->SetLineColor(kRed);
  LeadingEtaN2->SetLineColor(kRed);
  SubLeadingEtaN2->SetLineColor(kRed);
  //Legend 1
  TLegend* leg = new TLegend(0.15,0.65,0.53,0.85);
  leg->SetHeader("Pythia8_A14_NNPDF23LO");
  leg->AddEntry(LeadingEtaP,"EvtGen","l");
  leg->AddEntry(LeadingEtaP2,"EvtGen + Photospp","l");
  leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  //Legend 2
  TLegend* leg2 = new TLegend(0.45,0.65,0.83,0.85);
  leg2->SetHeader("Pythia8_A14_NNPDF23LO");
  leg2->AddEntry(LeadingEtaP,"EvtGen","l");
  leg2->AddEntry(LeadingEtaP2,"EvtGen + Photospp","l");
  leg2->SetFillStyle(0);
  leg2->SetBorderSize(0);
  //All decays
  TCanvas c0("c0","DCH Decay Types",1000,600);
  c0.cd();
  DecayType->Draw();
  DecayType2->Draw("same");
  DecayType2->SetLineStyle(7);
  leg->DrawClone();
  c0.Print("DCH_photospp_DecayTypes.pdf");
  c0.Close();
  //Positive Pair
  TCanvas c1("c1","Positive Lepton Pairs",1000,1200);
  c1.Divide(2,3);
  c1.cd(1);
  LeptonPairTypeP->Draw();
  LeptonPairTypeP2->Draw("same");
  LeptonPairTypeP2->SetLineStyle(7);
  leg->DrawClone();
  c1.cd(2);
  InvariantMassP->Draw();
  InvariantMassP2->Draw("same");
  leg->DrawClone();
  c1.cd(3);
  LeadingPtP->Draw();
  LeadingPtP2->Draw("same");
  leg2->DrawClone();
  c1.cd(4);
  SubLeadingPtP->Draw();
  SubLeadingPtP2->Draw("same");
  leg2->DrawClone();
  c1.cd(5);
  LeadingEtaP->Draw();
  LeadingEtaP2->Draw("same");
  leg->DrawClone();
  c1.cd(6);
  SubLeadingEtaP->Draw();
  SubLeadingEtaP2->Draw("same");
  leg->DrawClone();
  c1.SetCanvasSize(1000,1200);
  c1.Print("DCH_photospp_PositiveLeptonPairs.pdf");
  c1.Close();

  //Negative Pair
  TCanvas c2("c2","Negative Lepton Pairs",1000,1200);
  c2.Divide(2,3);
  c2.cd(1);
  LeptonPairTypeN->Draw();
  LeptonPairTypeN2->Draw("same");
  LeptonPairTypeN2->SetLineStyle(7);
  leg->DrawClone();
  c2.cd(2);
  InvariantMassN->Draw();
  InvariantMassN2->Draw("same");
  leg->DrawClone();
  c2.cd(3);
  LeadingPtN->Draw();
  LeadingPtN2->Draw("same");
  leg2->DrawClone();
  c2.cd(4);
  SubLeadingPtN->Draw();
  SubLeadingPtN2->Draw("same");
  leg2->DrawClone();
  c2.cd(5);
  LeadingEtaN->Draw();
  LeadingEtaN2->Draw("same");
  leg->DrawClone();
  c2.cd(6);
  SubLeadingEtaN->Draw();
  SubLeadingEtaN2->Draw("same");
  leg->DrawClone();
  c2.SetCanvasSize(1000,1200);
  gPad->Update(); 
  c2.Print("DCH_photospp_NegativeLeptonPairs.pdf");
  c2.Close();

}
#include <map>
#include <utility>
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::ostringstream
#include <iostream>
#include <fstream>
#include "TFile.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TLine.h"
#include "TLegend.h"
#include "TGraphErrors.h"

void DCHCrossSectionPlot() {
  ifstream file; file.open( "../macros/CrossSections.txt" );
  ifstream file2; file2.open( "../macros/CrossSections.txt" );
  double mass[11], HL[11], HLe[11], HR[11], HRe[11], TOT[11], TOTe[11];
  double mass2[11], HL2[11], HLe2[11], HR2[11], HRe2[11], TOT2[11], TOTe2[11];
  for(int i = 0; i < 11; i++){
    double temp;
    file >> mass[i] >> HL[i] >> HLe[i] >> HR[i] >> HRe[i] >> TOT[i] >> TOTe[i];
    if (i<5) file2 >> temp >> temp >> temp >> temp >> temp >> temp >> temp;
    if (i<5) {
      mass2[i+6]=1300;
      HL2[i+6]=5.990e-3;
      HLe2[i+6]=1.089e-5;
      HR2[i+6]=2.880e-3;
      HRe2[i+6]=7.591e-6;
      TOT2[i+6]=8.871e-3;
      TOTe2[i+6]=1.328e-5;
    }
    if(i>4) {
      file2 >> mass2[i-5] >> HL2[i-5] >> HLe2[i-5] >> HR2[i-5] >> HRe2[i-5] >> TOT2[i-5] >> TOTe2[i-5];
      HL2[i-5]*=1e12;
      HLe2[i-5]*=1e12;
      HR2[i-5]*=1e12;
      HRe2[i-5]*=1e12;
      TOT2[i-5]*=1e12;
      TOTe2[i-5]*=1e12;
    }
    HL[i]*=1e12;
    HLe[i]*=1e12;
    HR[i]*=1e12;
    HRe[i]*=1e12;
    TOT[i]*=1e12;
    TOTe[i]*=1e12;
    std::cout << mass[i] << "  " <<  HL[i] << "  " <<  HLe[i] << "  " <<  HR[i] << "  " <<  HRe[i] << "  " <<  TOT[i] << "  " <<  TOTe[i] << "  " <<  std::endl;

  }
  TCanvas* c1 = new TCanvas("c1","DCH Cross Section",1200,1100);
  c1->Divide(1,2,0.001,0.001);
  TGraphErrors* gHTOT = new TGraphErrors(11,mass,TOT,0,TOTe);
  TGraphErrors* gHL = new TGraphErrors(11,mass,HL,0,HLe);
  TGraphErrors* gHR = new TGraphErrors(11,mass,HR,0,HRe);

  TGraphErrors* gHTOT2 = new TGraphErrors(11,mass2,TOT2,0,TOTe2);
  TGraphErrors* gHL2 = new TGraphErrors(11,mass2,HL2,0,HLe2);
  TGraphErrors* gHR2 = new TGraphErrors(11,mass2,HR2,0,HRe2);

  gHTOT->GetXaxis()->SetTitle("DCH Mass (GeV)");
  gHTOT->GetYaxis()->SetTitle("Cross Section (fb)");
  gHTOT->SetTitle("");
  gHTOT->SetLineColor(kGreen+1);
  gHL->SetLineColor(kRed);
  gHR->SetLineColor(kBlue);

  gHTOT2->GetXaxis()->SetTitle("DCH Mass (GeV)");
  gHTOT2->GetYaxis()->SetTitle("Cross Section (fb)");
  gHTOT2->SetTitle("");
  gHTOT2->SetLineColor(kGreen+1);
  gHL2->SetLineColor(kRed);
  gHR2->SetLineColor(kBlue);

  TLegend* leg = new TLegend(0.57,0.650,0.9,0.85);
  leg->SetHeader("A14_NNPDF23LO_EvtGen");
  leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  leg->AddEntry(gHL,"f fbar -> H_L^++ H_L^--","le");
  leg->AddEntry(gHR,"f fbar -> H_R^++ H_R^--","le");
  leg->AddEntry(gHTOT,"sum","le");


  c1->cd(1);
  gHTOT->Draw("alp");
  gHL->Draw("same");
  gHR->Draw("same");
  leg->Draw();

  c1->cd(2);
  gHTOT2->Draw("alp");
  gHL2->Draw("same");
  gHR2->Draw("same");


  c1->Print("ValidationPlots/GeneralPlots/DCH_CrossSection.pdf");

}
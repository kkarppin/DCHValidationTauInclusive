#include <map>
#include <utility>
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::ostringstream
#include "TFile.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TLine.h"
#include "TROOT.h"
#include "TLegend.h"

void DCHValidationPlots(double m_dch) {
  gROOT->SetBatch(kTRUE);

 //ostringstream fileName; fileName << "/afs/cern.ch/user/s/sarnling/DCHSignalSample/histograms/DCH" << m_dch << "AtLeastOneTauFilter.UndecayedTaus.Validation.histo.root";
  
  ostringstream fileName; fileName << "/afs/cern.ch/user/s/sarnling/DCHSignalSample/histograms/DCH300FourTauHad.Validation.histo.root";
 
 
 
 
 
 
  TFile*  file  =  new  TFile( fileName.str().c_str() );

  /********* Get Pointers to the Histograms *********/
  //General Histograms
  TH1D*    DecayType  =  (TH1D*)  file->Get("DCHValidationHistograms/DecayType");
  TH1D*    FinalStateLeptons  =  (TH1D*)  file->Get("DCHValidationHistograms/FinalStateLeptons");  
  TH1D*    NonDCHTauParents  =  (TH1D*)  file->Get("DCHValidationHistograms/NonDCHTauParents");
  TH1D*    Electrons  =  (TH1D*)  file->Get("DCHValidationHistograms/Electrons");
  TH1D*    Muons  =  (TH1D*)  file->Get("DCHValidationHistograms/Muons");
  TH1D*    Tauons  =  (TH1D*)  file->Get("DCHValidationHistograms/Tauons");
  
  //General Pairs of Leptons
  TH1D*    LeptonPairTypeP  =  (TH1D*)  file->Get("DCHValidationHistograms/LeptonPairTypeP");
  TH1D*    LeptonPairTypeN  =  (TH1D*)  file->Get("DCHValidationHistograms/LeptonPairTypeN");
  TH1D*    InvariantMassP  =  (TH1D*)  file->Get("DCHValidationHistograms/InvariantMassP");
  TH1D*    InvariantMassN  =  (TH1D*)  file->Get("DCHValidationHistograms/InvariantMassN");
  TH1D*    LeadingPtP  =  (TH1D*)  file->Get("DCHValidationHistograms/LeadingPtP");
  TH1D*    SubLeadingPtP  =  (TH1D*)  file->Get("DCHValidationHistograms/SubLeadingPtP");
  TH1D*    LeadingEtaP  =  (TH1D*)  file->Get("DCHValidationHistograms/LeadingEtaP");
  TH1D*    SubLeadingEtaP  =  (TH1D*)  file->Get("DCHValidationHistograms/SubLeadingEtaP");
  TH1D*    LeadingPtN  =  (TH1D*)  file->Get("DCHValidationHistograms/LeadingPtN");
  TH1D*    SubLeadingPtN  =  (TH1D*)  file->Get("DCHValidationHistograms/SubLeadingPtN");
  TH1D*    LeadingEtaN  =  (TH1D*)  file->Get("DCHValidationHistograms/LeadingEtaN");
  TH1D*    SubLeadingEtaN  =  (TH1D*)  file->Get("DCHValidationHistograms/SubLeadingEtaN");

  /********* Histogram Axis Titles *********/
  //Decay  Type
  DecayType->SetNameTitle("DecayType","H^{#pm#pm} #rightarrow l^{#pm}l^{#pm} Decay Modes");
  DecayType->GetYaxis()->SetTitle("Events");
  DecayType->GetYaxis()->SetTitleOffset(1.4);
  
  DecayType->GetXaxis()->SetLabelSize(0.06);
  DecayType->GetXaxis()->SetBinLabel(1,"Other");
  DecayType->GetXaxis()->SetBinLabel(2,"2e");
  DecayType->GetXaxis()->SetBinLabel(3,"2m");
  DecayType->GetXaxis()->SetBinLabel(4,"2t");
  DecayType->GetXaxis()->SetBinLabel(5,"em");
  DecayType->GetXaxis()->SetBinLabel(6,"tm");
  DecayType->GetXaxis()->SetBinLabel(7,"te");
  DecayType->GetXaxis()->SetBinLabel(8,"WW");
  
  //Final State Leptons
  FinalStateLeptons->GetYaxis()->SetTitle("Events");
  FinalStateLeptons->GetYaxis()->SetTitleOffset(1.4);
  FinalStateLeptons->GetXaxis()->SetTitleOffset(1.1);
  FinalStateLeptons->SetNameTitle("FinalStateLeptons","H^{#pm#pm}H^{#mp#mp} #rightarrow l^{#pm}l^{#pm}l^{#mp}l^{#mp} 4-lepton final states");
  FinalStateLeptons->GetXaxis()->SetTitle("4-lepton final states");
  
  FinalStateLeptons->GetXaxis()->SetLabelSize(0.04);
  FinalStateLeptons->GetXaxis()->SetBinLabel(1,"Other");
  FinalStateLeptons->GetXaxis()->SetBinLabel(2,"4e");
  FinalStateLeptons->GetXaxis()->SetBinLabel(3,"2e/2m");
  FinalStateLeptons->GetXaxis()->SetBinLabel(4,"2e/2t");
  FinalStateLeptons->GetXaxis()->SetBinLabel(5,"2e/em");
  FinalStateLeptons->GetXaxis()->SetBinLabel(6,"2e/tm");
  FinalStateLeptons->GetXaxis()->SetBinLabel(7,"2e/te");
  FinalStateLeptons->GetXaxis()->SetBinLabel(8,"4m");
  FinalStateLeptons->GetXaxis()->SetBinLabel(9,"2m/2t");
  FinalStateLeptons->GetXaxis()->SetBinLabel(10,"2m/em");
  FinalStateLeptons->GetXaxis()->SetBinLabel(11,"2m/tm");
  FinalStateLeptons->GetXaxis()->SetBinLabel(12,"2m/te");
  FinalStateLeptons->GetXaxis()->SetBinLabel(13,"4t");
  std::cout << std::endl << "Numer of true 4tau events" << FinalStateLeptons->GetBinContent(13) << std::endl;
  FinalStateLeptons->GetXaxis()->SetBinLabel(14,"2t/em");
  FinalStateLeptons->GetXaxis()->SetBinLabel(15,"2t/tm");
  FinalStateLeptons->GetXaxis()->SetBinLabel(16,"2t/te");
  FinalStateLeptons->GetXaxis()->SetBinLabel(17,"em/em");
  FinalStateLeptons->GetXaxis()->SetBinLabel(18,"em/tm");
  FinalStateLeptons->GetXaxis()->SetBinLabel(19,"em/te");
  FinalStateLeptons->GetXaxis()->SetBinLabel(20,"tm/tm");
  FinalStateLeptons->GetXaxis()->SetBinLabel(21,"te/tm");
  FinalStateLeptons->GetXaxis()->SetBinLabel(22,"te/te");
  
  gStyle->SetLegendTextSize(0.);
	TObject* nullPtr = NULL;  
  TLegend* legend = new TLegend(0.82,0.71,0.98,0.88);
  legend->AddEntry(nullPtr,"e  =  e/#tau_{e}    ","");
  legend->AddEntry(nullPtr,"m  =  #mu/#tau_{#mu}     ","");
  legend->AddEntry(nullPtr,"t  =  #tau_{had}    ","");
  
   

	//NonDCHTauParents
	
	//Convert NonDCHTauParents to equal binning
  TH1D* NonDCHTauParentsBinning = new TH1D( "NonDCHTauParentsBinning", "NonDCHTauParentsBinning", 7, 0, 7);
  NonDCHTauParentsBinning->SetBinContent(1,NonDCHTauParents->GetBinContent(1)); //Other
  NonDCHTauParentsBinning->SetBinContent(2,NonDCHTauParents->GetBinContent(23)); //Photon
  NonDCHTauParentsBinning->SetBinContent(3,NonDCHTauParents->GetBinContent(412)); //D+
  NonDCHTauParentsBinning->SetBinContent(4,NonDCHTauParents->GetBinContent(432)); //D_S+
  NonDCHTauParentsBinning->SetBinContent(5,NonDCHTauParents->GetBinContent(512)); //B0
  NonDCHTauParentsBinning->SetBinContent(6,NonDCHTauParents->GetBinContent(522)); //B+
  NonDCHTauParentsBinning->SetBinContent(7,NonDCHTauParents->GetBinContent(532)); //B_s0
	
	NonDCHTauParentsBinning->SetEntries(NonDCHTauParentsBinning->Integral());
	
	NonDCHTauParentsBinning->GetYaxis()->SetTitle("Events");
  NonDCHTauParentsBinning->GetYaxis()->SetTitleOffset(1.0);
  NonDCHTauParentsBinning->SetNameTitle("NonDCHTauParents","Non-H^{#pm#pm} parents for #tau-leptons");
  NonDCHTauParentsBinning->GetXaxis()->SetTitle("Parent particle");
  NonDCHTauParentsBinning->GetXaxis()->SetTitleOffset(1.3);
  
  NonDCHTauParentsBinning->GetXaxis()->SetLabelSize(0.06);
  NonDCHTauParentsBinning->GetXaxis()->SetBinLabel(1,"Other");    
  NonDCHTauParentsBinning->GetXaxis()->SetBinLabel(2,"#gamma");
  NonDCHTauParentsBinning->GetXaxis()->SetBinLabel(3,"D^{#pm}");
  NonDCHTauParentsBinning->GetXaxis()->SetBinLabel(4,"D_{s}^{#pm}");
  NonDCHTauParentsBinning->GetXaxis()->SetBinLabel(5,"B^{0}");
  NonDCHTauParentsBinning->GetXaxis()->SetBinLabel(6,"B^{#pm}");
  NonDCHTauParentsBinning->GetXaxis()->SetBinLabel(7,"B_{s}^{0}");



  //Electrons, Muons, Tauons
  Electrons->GetYaxis()->SetTitle("Events");
  Electrons->GetYaxis()->SetTitleOffset(1.4);
  Electrons->GetXaxis()->SetBinLabel(1,"Undecayed");
  Electrons->GetXaxis()->SetBinLabel(2,"Decayed");
    
  Muons->GetYaxis()->SetTitle("Events");
  Muons->GetYaxis()->SetTitleOffset(1.4);
  Muons->GetXaxis()->SetBinLabel(1,"Undecayed");
  Muons->GetXaxis()->SetBinLabel(2,"Decayed"); 
  
  Tauons->GetYaxis()->SetTitle("Events");
  Tauons->GetYaxis()->SetTitleOffset(1.4);
  Tauons->GetXaxis()->SetBinLabel(1,"Undecayed");
  Tauons->GetXaxis()->SetBinLabel(2,"Decayed");
   

  //Positive  Lepton  Pair  Type
  LeptonPairTypeP->GetYaxis()->SetTitle("Events");
  LeptonPairTypeP->GetYaxis()->SetTitleOffset(1.4);
  LeptonPairTypeP->GetXaxis()->SetLabelSize(0.06);  
  LeptonPairTypeP->GetXaxis()->SetBinLabel(1,"Other");
  LeptonPairTypeP->GetXaxis()->SetBinLabel(2,"2e");
  LeptonPairTypeP->GetXaxis()->SetBinLabel(3,"2m");
  LeptonPairTypeP->GetXaxis()->SetBinLabel(4,"2t");
  LeptonPairTypeP->GetXaxis()->SetBinLabel(5,"em");
  LeptonPairTypeP->GetXaxis()->SetBinLabel(6,"mt");
  LeptonPairTypeP->GetXaxis()->SetBinLabel(7,"et");
  LeptonPairTypeP->GetXaxis()->SetBinLabel(8,"WW");
  LeptonPairTypeP->SetAxisRange(0,7);

  //Negative  Lepton  Pair  Type
  LeptonPairTypeN->GetYaxis()->SetTitle("Events");
  LeptonPairTypeN->GetYaxis()->SetTitleOffset(1.4);  
  LeptonPairTypeN->GetXaxis()->SetLabelSize(0.06);
  LeptonPairTypeN->GetXaxis()->SetBinLabel(1,"Other");
  LeptonPairTypeN->GetXaxis()->SetBinLabel(2,"2e");
  LeptonPairTypeN->GetXaxis()->SetBinLabel(3,"2m");
  LeptonPairTypeN->GetXaxis()->SetBinLabel(4,"2t");
  LeptonPairTypeN->GetXaxis()->SetBinLabel(5,"em");
  LeptonPairTypeN->GetXaxis()->SetBinLabel(6,"mt");
  LeptonPairTypeN->GetXaxis()->SetBinLabel(7,"et");
  LeptonPairTypeN->GetXaxis()->SetBinLabel(8,"WW");

  ////////////////////////////////////////////////////////////////////////////////
  //Positive  pair  invariant  mass
  InvariantMassP->GetXaxis()->SetTitle("m(l^{+} l^{+}) [GeV]");
  std::ostringstream ppm;
  ppm << "pairs/(" << InvariantMassP->GetBinWidth(0) << " GeV)";
  InvariantMassP->GetYaxis()->SetTitle(ppm.str().c_str());

  //Negative  pair  invariant  mass
  InvariantMassN->GetXaxis()->SetTitle("m(l^{-} l^{-}) [GeV]");
  std::ostringstream npm;
  npm << "pairs/(" << InvariantMassN->GetBinWidth(0) << " GeV)";
  InvariantMassN->GetYaxis()->SetTitle(npm.str().c_str());

  //Positive leading lepton pt
  LeadingPtP->GetXaxis()->SetTitle("p_{T} [GeV]");
  std::ostringstream pllp;
  pllp << "leptons/(" << LeadingPtP->GetBinWidth(0) << " GeV)";
  LeadingPtP->GetYaxis()->SetTitle(pllp.str().c_str());

  //Negative leading lepton pt
  LeadingPtN->GetXaxis()->SetTitle("p_{T} [GeV]");
  std::ostringstream nllp;
  nllp << "leptons/(" << LeadingPtN->GetBinWidth(0) << " GeV)";
  LeadingPtN->GetYaxis()->SetTitle(nllp.str().c_str());

  //Positive sub-leading lepton pt
  SubLeadingPtP->GetXaxis()->SetTitle("p_{T} [GeV]");
  std::ostringstream psllp;
  psllp << "leptons/(" << SubLeadingPtP->GetBinWidth(0) << " GeV)";
  SubLeadingPtP->GetYaxis()->SetTitle(psllp.str().c_str());
  std::cout << SubLeadingPtP->GetYaxis()->GetTitleOffset() << std::endl;

  //Negative sub-leading lepton pt
  SubLeadingPtN->GetXaxis()->SetTitle("p_{T} [GeV]");
  std::ostringstream nsllp;
  nsllp << "leptons/(" << SubLeadingPtN->GetBinWidth(0) << " GeV)";
  SubLeadingPtN->GetYaxis()->SetTitle(nsllp.str().c_str());

  //Positive leading lepton eta
  LeadingEtaP->GetXaxis()->SetTitle("#eta");
  std::ostringstream plle;
  plle << "leptons/(" << LeadingEtaP->GetBinWidth(0) << ")";
  LeadingEtaP->GetYaxis()->SetTitle(plle.str().c_str());

  //Negative leading lepton pt
  LeadingEtaN->GetXaxis()->SetTitle("#eta");
  std::ostringstream nlle;
  nlle << "leptons/(" << LeadingEtaN->GetBinWidth(0) << ")";
  LeadingEtaN->GetYaxis()->SetTitle(nlle.str().c_str());

  //Positive sub-leading lepton pt
  SubLeadingEtaP->GetXaxis()->SetTitle("#eta");
  std::ostringstream pslle;
  pslle << "leptons/(" << SubLeadingEtaP->GetBinWidth(0) << ")";
  SubLeadingEtaP->GetYaxis()->SetTitle(pslle.str().c_str());

  //Negative sub-leading lepton pt
  SubLeadingEtaN->GetXaxis()->SetTitle("#eta");
  std::ostringstream nslle;
  nslle << "leptons/(" << SubLeadingEtaN->GetBinWidth(0) << ")";
  SubLeadingEtaN->GetYaxis()->SetTitle(nslle.str().c_str());




  /********* Draw Canvases and Save to File *********/
  //General Histograms
  
    //Final state leptons
  TCanvas c40("c0","DCH Final state lepton combinations",1600,1000);
  c40.cd();
  gStyle->SetOptStat("e");
  FinalStateLeptons->Draw("E1");
  legend->Draw();
  ostringstream printName40; printName40 << "ValidationPlots/DCH" << m_dch << "_FinalStateLeptons.pdf";
  ostringstream printName41; printName41 << "ValidationPlots/DCH" << m_dch << "_ValidationPlots.pdf(";
  c40.Print( printName40.str().c_str() );
  c40.Print( printName41.str().c_str() );
  
  //NonDCHTauParents
  TCanvas c41("c0","NonDCHTauParents",1600,1000);
  c41.cd();
  c41.SetLogy();
  gStyle->SetOptStat("e");
  NonDCHTauParentsBinning->Draw("E1");
  ostringstream printName42; printName42 << "ValidationPlots/DCH" << m_dch << "NonDCHTauParents.pdf";
  ostringstream printName43; printName43 << "ValidationPlots/DCH" << m_dch << "_ValidationPlots.pdf(";
  c41.Print( printName42.str().c_str() );
  c41.Print( printName43.str().c_str() );
  
  //DecayType
  
  TCanvas c42("c0","DCH decay type",1600,1000);
  c42.cd();
  gStyle->SetOptStat("e");
  DecayType->Draw("E1"); 
  legend->Draw();
  ostringstream printName44; printName44 << "ValidationPlots/DCH" << m_dch << "_DecayType.pdf";
  ostringstream printName45; printName45 << "ValidationPlots/DCH" << m_dch << "_ValidationPlots.pdf(";
  c42.Print( printName44.str().c_str() );
  c42.Print( printName45.str().c_str() ); 
  
  //general histograms
  
  TCanvas c0("c0","DCH General Histograms",1600,1000);
  gPad->SetLeftMargin(1);
  
  
  c0.Divide(3,2);
  c0.cd(1);
  TLatex text;
  text.SetTextAlign(22);
  text.SetTextSize(0.05);
  text.DrawLatex(0.5,0.75,"Doubly Charged Higgs to leptons.");
  text.DrawLatex(0.5,0.70,"Pythia8 Tune: A14_NNPDF23LO_EvtGen");
  ostringstream textHiggsMass; textHiggsMass << "DCH Mass: " << m_dch << " GeV";
  text.DrawLatex(0.5,0.65, textHiggsMass.str().c_str() );
  c0.cd(2); DecayType->Draw();
  c0.cd(3); FinalStateLeptons->Draw(); 
  c0.cd(4); Electrons->Draw();
  c0.cd(5); Muons->Draw();
  c0.cd(6); Tauons->Draw();
  ostringstream printName1; printName1 << "ValidationPlots/DCH" << m_dch << "_GeneralHistograms.pdf";
  ostringstream printName2; printName2 << "ValidationPlots/DCH" << m_dch << "_ValidationPlots.pdf(";
  c0.Print( printName1.str().c_str() );
  c0.Print( printName2.str().c_str() );
  


  //Positive Pair
  TCanvas c1("c1","Positive Lepton Pairs",1600,1000);
  c1.Divide(3,2);
  c1.cd(1); LeptonPairTypeP->Draw();
  c1.cd(4); InvariantMassP->Draw();
  c1.cd(2); LeadingPtP->Draw();
  c1.cd(5); SubLeadingPtP->Draw();
  c1.cd(3); LeadingEtaP->Draw();
  c1.cd(6); SubLeadingEtaP->Draw();
  gPad->Update(); 
  ostringstream printName3; printName3 << "ValidationPlots/DCH" << m_dch << "_Positive_Lepton_Pairs.pdf";
  ostringstream printName4; printName4 << "ValidationPlots/DCH" << m_dch << "_ValidationPlots.pdf";
  c1.Print( printName3.str().c_str() );
  c1.Print( printName4.str().c_str() );

  //Negative Pair
  TCanvas c2("c2","Negative Lepton Pairs",1600,1000);
  c2.Divide(3,2);
  c2.cd(1); LeptonPairTypeN->Draw();
  c2.cd(4); InvariantMassN->Draw();
  c2.cd(2); LeadingPtN->Draw();
  c2.cd(5); SubLeadingPtN->Draw();
  c2.cd(3); LeadingEtaN->Draw();
  c2.cd(6); SubLeadingEtaN->Draw();
  gPad->Update(); 
  ostringstream printName5; printName5 << "ValidationPlots/DCH" << m_dch << "_Negative_Lepton_Pairs.pdf";
  c2.Print( printName5.str().c_str() );
  c2.Print( printName4.str().c_str() );

/************************************** Specific Histograms **********************************/
  TCanvas* specificCanvasPositive;
  TCanvas* specificCanvasNegative;
  std::string m_shortNamePool[6] = {"e" , "mu", "tau", "emu", "mutau", "etau"};
  std::string m_feynmanDiagName1[6] = {"e/#tau_{e}" , "#mu/#tau_{#mu}", "#tau_{had}", "e/#tau_{e}", "#mu/#tau_{#mu}", "e/#tau_{e}"};
  std::string m_feynmanDiagName2[6] = {"e/#tau_{e}" , "#mu/#tau_{#mu}", "#tau_{had}", "#mu/#tau_{#mu}", "#tau_{had}", "#tau_{had}"};
  std::string m_leptonNames[6] = {"Electron-Electron", "Muon-Muon", "Tauon-Tauon", "Electron-Muon", "Muon-Tauon", "Electron-Tauon"};
  std::string m_shortLeptonNegativeNames[6] = {"e- e-", "#mu- #mu-", "#tau- #tau-", "e- #mu-", "#mu- #tau-", "e- #tau-"};
  std::string m_shortLeptonPositiveNames[6] = {"e+ e+", "#mu+ #mu+", "#tau+ #tau+", "e+ #mu+", "#mu+ #tau+", "e+ #tau+"};
  std::map <std::string, TH1D*> specificHistograms;
  for (int i = 0; i < 6; i++) {
    std::ostringstream osInvariantMassP; osInvariantMassP << "DCHValidationHistograms/InvariantMassP" << m_shortNamePool[i];
    std::ostringstream osInvariantMassN; osInvariantMassN << "DCHValidationHistograms/InvariantMassN" << m_shortNamePool[i];
    std::ostringstream osLeadingPtP    ; osLeadingPtP     << "DCHValidationHistograms/LeadingPtP"     << m_shortNamePool[i];
    std::ostringstream osSubLeadingPtP ; osSubLeadingPtP  << "DCHValidationHistograms/SubLeadingPtP"  << m_shortNamePool[i];
    std::ostringstream osLeadingEtaP   ; osLeadingEtaP    << "DCHValidationHistograms/LeadingEtaP"    << m_shortNamePool[i];
    std::ostringstream osSubLeadingEtaP; osSubLeadingEtaP << "DCHValidationHistograms/SubLeadingEtaP" << m_shortNamePool[i];
    std::ostringstream osLeadingPtN    ; osLeadingPtN     << "DCHValidationHistograms/LeadingPtN"     << m_shortNamePool[i];
    std::ostringstream osSubLeadingPtN ; osSubLeadingPtN  << "DCHValidationHistograms/SubLeadingPtN"  << m_shortNamePool[i];
    std::ostringstream osLeadingEtaN   ; osLeadingEtaN    << "DCHValidationHistograms/LeadingEtaN"    << m_shortNamePool[i];
    std::ostringstream osSubLeadingEtaN; osSubLeadingEtaN << "DCHValidationHistograms/SubLeadingEtaN" << m_shortNamePool[i];

    specificHistograms.insert( std::pair<std::string,TH1D*>( osInvariantMassP.str(), (TH1D*)file->Get( osInvariantMassP.str().c_str() ) ) );
    specificHistograms.insert( std::pair<std::string,TH1D*>( osInvariantMassN.str(), (TH1D*)file->Get( osInvariantMassN.str().c_str() ) ) );
    specificHistograms.insert( std::pair<std::string,TH1D*>( osLeadingPtP.str()    , (TH1D*)file->Get( osLeadingPtP.str().c_str()     ) ) );
    specificHistograms.insert( std::pair<std::string,TH1D*>( osSubLeadingPtP.str() , (TH1D*)file->Get( osSubLeadingPtP.str().c_str()  ) ) );
    specificHistograms.insert( std::pair<std::string,TH1D*>( osLeadingEtaP.str()   , (TH1D*)file->Get( osLeadingEtaP.str().c_str()    ) ) );
    specificHistograms.insert( std::pair<std::string,TH1D*>( osSubLeadingEtaP.str(), (TH1D*)file->Get( osSubLeadingEtaP.str().c_str() ) ) );
    specificHistograms.insert( std::pair<std::string,TH1D*>( osLeadingPtN.str()    , (TH1D*)file->Get( osLeadingPtN.str().c_str()     ) ) );
    specificHistograms.insert( std::pair<std::string,TH1D*>( osSubLeadingPtN.str() , (TH1D*)file->Get( osSubLeadingPtN.str().c_str()  ) ) );
    specificHistograms.insert( std::pair<std::string,TH1D*>( osLeadingEtaN.str()   , (TH1D*)file->Get( osLeadingEtaN.str().c_str()    ) ) );
    specificHistograms.insert( std::pair<std::string,TH1D*>( osSubLeadingEtaN.str(), (TH1D*)file->Get( osSubLeadingEtaN.str().c_str() ) ) );

    //Positive  pair  invariant  mass
    std::ostringstream title1; title1 << "Positive " << m_leptonNames[i] << " Pair Invariant Mass";
    std::ostringstream xtitle1; xtitle1 << "m(" << m_shortLeptonPositiveNames[i] << ") [GeV]";
    specificHistograms[ osInvariantMassP.str().c_str() ]->GetXaxis()->SetTitle( xtitle1.str().c_str() );
    specificHistograms[ osInvariantMassP.str().c_str() ]->SetTitle( title1.str().c_str() );
    std::ostringstream ppm1;
    ppm1 << "pairs/(" << InvariantMassP->GetBinWidth(0) << " GeV)";
    specificHistograms[ osInvariantMassP.str().c_str() ]->GetYaxis()->SetTitle(ppm1.str().c_str());

    //Negative  pair  invariant  mass
    std::ostringstream title2; title2 << "Negative " << m_leptonNames[i] << " Pair Invariant Mass";
    std::ostringstream xtitle2; xtitle2 << "m(" << m_shortLeptonNegativeNames[i] << ") [GeV]";
    specificHistograms[ osInvariantMassN.str().c_str() ]->GetXaxis()->SetTitle( xtitle2.str().c_str() );
    specificHistograms[ osInvariantMassN.str().c_str() ]->SetTitle( title2.str().c_str() );
    std::ostringstream npm1;
    npm1 << "pairs/(" << InvariantMassN->GetBinWidth(0) << " GeV)";
    specificHistograms[ osInvariantMassN.str().c_str() ]->GetYaxis()->SetTitle(npm1.str().c_str());

    //Positive leading lepton pt
    std::ostringstream title3; title3 << "Positive Leading " << m_leptonNames[i] << " p_{T}";
    specificHistograms[ osLeadingPtP.str().c_str() ]->SetTitle( title3.str().c_str() );
    specificHistograms[ osLeadingPtP.str().c_str() ]->GetXaxis()->SetTitle("p_{T} [GeV]");
    std::ostringstream pllp1;
    pllp1 << "leptons/(" << LeadingPtP->GetBinWidth(0) << " GeV)";
    specificHistograms[ osLeadingPtP.str().c_str() ]->GetYaxis()->SetTitle(pllp1.str().c_str());

    //Negative leading lepton pt
    std::ostringstream title4; title4 << "Negative Leading " << m_leptonNames[i] << " p_{T}";
    specificHistograms[ osLeadingPtN.str().c_str() ]->SetTitle( title4.str().c_str() );
    specificHistograms[ osLeadingPtN.str().c_str() ]->GetXaxis()->SetTitle("p_{T} [GeV]");
    std::ostringstream nllp1;
    nllp1 << "leptons/(" << LeadingPtN->GetBinWidth(0) << " GeV)";
    specificHistograms[ osLeadingPtN.str().c_str() ]->GetYaxis()->SetTitle(nllp1.str().c_str());

    //Positive sub-leading lepton pt
    std::ostringstream title5; title5 << "Positive Sub-Leading " << m_leptonNames[i] << " p_{T}";
    specificHistograms[ osSubLeadingPtP.str().c_str() ]->SetTitle( title5.str().c_str() );
    specificHistograms[ osSubLeadingPtP.str().c_str() ]->GetXaxis()->SetTitle("p_{T} [GeV]");
    std::ostringstream psllp1;
    psllp1 << "leptons/(" << SubLeadingPtP->GetBinWidth(0) << " GeV)";
    specificHistograms[ osSubLeadingPtP.str().c_str() ]->GetYaxis()->SetTitle(psllp1.str().c_str());

    //Negative sub-leading lepton pt
    std::ostringstream title6; title6 << "Negative Sub-Leading " << m_leptonNames[i] << " p_{T}";
    specificHistograms[ osSubLeadingPtN.str().c_str() ]->SetTitle( title6.str().c_str() );
    specificHistograms[ osSubLeadingPtN.str().c_str() ]->GetXaxis()->SetTitle("p_{T} [GeV]");
    std::ostringstream nsllp1;
    nsllp1 << "leptons/(" << SubLeadingPtN->GetBinWidth(0) << " GeV)";
    specificHistograms[ osSubLeadingPtN.str().c_str() ]->GetYaxis()->SetTitle(nsllp1.str().c_str());

    //Positive leading lepton eta
    std::ostringstream title7; title7 << "Positive Leading " << m_leptonNames[i] << " #eta";
    specificHistograms[ osLeadingEtaP.str().c_str() ]->SetTitle( title7.str().c_str() );
    specificHistograms[ osLeadingEtaP.str().c_str() ]->GetXaxis()->SetTitle("#eta");
    std::ostringstream plle1;
    plle1 << "leptons/(" << LeadingEtaP->GetBinWidth(0) << ")";
    specificHistograms[ osLeadingEtaP.str().c_str() ]->GetYaxis()->SetTitle(plle1.str().c_str());

    //Negative leading lepton eta
    std::ostringstream title8; title8 << "Negative Leading " << m_leptonNames[i] << " #eta";
    specificHistograms[ osLeadingEtaN.str().c_str() ]->SetTitle( title7.str().c_str() );
    specificHistograms[ osLeadingEtaN.str().c_str() ]->GetXaxis()->SetTitle("#eta");
    std::ostringstream nlle1;
    nlle1 << "leptons/(" << LeadingEtaN->GetBinWidth(0) << ")";
    specificHistograms[ osLeadingEtaN.str().c_str() ]->GetYaxis()->SetTitle(nlle1.str().c_str());

    //Positive sub-leading lepton eta
    std::ostringstream title9; title9 << "Positive Sub-Leading " << m_leptonNames[i] << " #eta";
    specificHistograms[ osSubLeadingEtaP.str().c_str() ]->SetTitle( title9.str().c_str() );
    specificHistograms[ osSubLeadingEtaP.str().c_str() ]->GetXaxis()->SetTitle("#eta");
    std::ostringstream pslle1;
    pslle1 << "leptons/(" << SubLeadingEtaP->GetBinWidth(0) << ")";
    specificHistograms[ osSubLeadingEtaP.str().c_str() ]->GetYaxis()->SetTitle(pslle1.str().c_str());

    //Negative sub-leading lepton eta
    std::ostringstream title10; title10 << "Negative Sub-Leading " << m_leptonNames[i] << " #eta";
    specificHistograms[ osSubLeadingEtaN.str().c_str() ]->SetTitle( title10.str().c_str() );
    specificHistograms[ osSubLeadingEtaN.str().c_str() ]->GetXaxis()->SetTitle("#eta");
    std::ostringstream nslle1;
    nslle1 << "leptons/(" << SubLeadingEtaN->GetBinWidth(0) << ")";
    specificHistograms[ osSubLeadingEtaN.str().c_str() ]->GetYaxis()->SetTitle(nslle1.str().c_str());


    std::ostringstream canvasName; canvasName << "Specific Lepton Pairs " << m_shortNamePool[i];
    specificCanvasPositive = new TCanvas("specificCanvasPositive", "DCH", 1600, 1000);
    specificCanvasNegative = new TCanvas("specificCanvasNegative", "DCH", 1600, 1000);
    specificCanvasPositive->Divide(3,2);
    specificCanvasNegative->Divide(3,2);
    /*************** Feynman Diagram ***************/
    specificCanvasPositive->cd(1);
    TLatex t;
    t.SetTextAlign(22);
    t.SetTextSize(0.07);
    TLine * l;
    l = new TLine(0.25, 0.50, 0.50, 0.50); l->SetLineStyle(2); l->Draw();
    l = new TLine(0.50, 0.50, 0.75, 0.75); l->Draw();
    l = new TLine(0.50, 0.50, 0.75, 0.25); l->Draw();
    std::ostringstream lepton1; lepton1 << m_feynmanDiagName1[i] << "^{+}";
    std::ostringstream lepton2; lepton2 << m_feynmanDiagName2[i] << "^{+}";
    t.DrawLatex(0.375,0.54,"H^{++}");
    t.DrawLatex(0.77,0.79,lepton1.str().c_str());
    t.DrawLatex(0.77,0.21,lepton2.str().c_str());

    specificCanvasNegative->cd(1);
    l = new TLine(0.25, 0.50, 0.50, 0.50); l->SetLineStyle(2); l->Draw();
    l = new TLine(0.50, 0.50, 0.75, 0.75); l->Draw();
    l = new TLine(0.50, 0.50, 0.75, 0.25); l->Draw();
    std::ostringstream lepton3; lepton3 << m_feynmanDiagName1[i] << "^{-}";
    std::ostringstream lepton4; lepton4 << m_feynmanDiagName2[i] << "^{-}";
    t.DrawLatex(0.375,0.54,"H^{--}");
    t.DrawLatex(0.77,0.79,lepton3.str().c_str());
    t.DrawLatex(0.77,0.21,lepton4.str().c_str());
    /*************** Feynman Diagram ***************/
    specificCanvasPositive->cd(4); specificHistograms[ osInvariantMassP.str().c_str() ]->Draw();
    specificCanvasNegative->cd(4); specificHistograms[ osInvariantMassN.str().c_str() ]->Draw();
    specificCanvasPositive->cd(2); specificHistograms[ osLeadingPtP.str().c_str() ]->Draw();
    specificCanvasPositive->cd(5); specificHistograms[ osSubLeadingPtP.str().c_str() ]->Draw();
    specificCanvasPositive->cd(3); specificHistograms[ osLeadingEtaP.str().c_str() ]->Draw();
    specificCanvasPositive->cd(6); specificHistograms[ osSubLeadingEtaP.str().c_str() ]->Draw();
    specificCanvasNegative->cd(2); specificHistograms[ osLeadingPtN.str().c_str() ]->Draw();
    specificCanvasNegative->cd(5); specificHistograms[ osSubLeadingPtN.str().c_str() ]->Draw();
    specificCanvasNegative->cd(3); specificHistograms[ osLeadingEtaN.str().c_str() ]->Draw();
    specificCanvasNegative->cd(6); specificHistograms[ osSubLeadingEtaN.str().c_str() ]->Draw();
    std::ostringstream printNamePositive; printNamePositive << "ValidationPlots/DCH" << m_dch << "_Positive_" << m_leptonNames[i] << "_Pairs.pdf";
    std::ostringstream printNameNegative; printNameNegative << "ValidationPlots/DCH" << m_dch << "_Negative_" << m_leptonNames[i] << "_Pairs.pdf";
    specificCanvasPositive->Print( printNamePositive.str().c_str() );
    specificCanvasNegative->Print( printNameNegative.str().c_str() ); //e.g DCH -> e- e- only has ~11000 events compared to 25000 events  
    if (i==5){
    specificCanvasPositive->Print( printName4.str().c_str() );
    ostringstream printName6; printName6 << "ValidationPlots/DCH" << m_dch << "_ValidationPlots.pdf)";
    specificCanvasNegative->Print( printName6.str().c_str() );
    }
    else{
    specificCanvasPositive->Print( printName4.str().c_str() );
    specificCanvasNegative->Print( printName4.str().c_str() );
    }

  }

}


GENERATING EVNT FILES
=======================

Setup:
	mkdir DCHSignalSample
	cd DCHSignalSample
	asetup 19.2.5,here     //Or alternative Athena setup. At time of writing, athena 19.2.5.27 was used for EVNT generation
	lsetup panda           //For grid submission


COMMANDS
========
To generate EVNT files locally:

	Generate_tf.py --ecmEnergy=13000 --firstEvent=1 --randomSeed=1234 --runNumber=301528 --maxEvents=10 --jobConfig=DCH300.py --outputEVNTFile=DCH300.EVNT.pool.root
	
--jobConfig needs a JobOptions file path as input. Examples of Pythia8 job options are given below, or can be found online.


To generate EVNT using the grid:
pathena --trf="Generate_tf.py --ecmEnergy=13000 --runNumber=301528 --firstEvent=1 --maxEvents=100000 --randomSeed=%RNDM:100 --jobConfig=DCH300.py --outputEVNTFile=%OUT.test.pool.root" --extFile=DCH300.py --nFilesPerJob=10 --split 10 --outDS user.kkarppin.13TeV.MC15.PowhegPythiaEvtGen_DCH300/




EXAMPLE OF DCH JOBOPTIONS FILE 
==========
DCH300.py: example /afs/cern.ch/work/k/kkarppin/public/Simon/DCH300.py
Pythia parameters: http://home.thep.lu.se/Pythia/pythia82html/LeftRightSymmetryProcesses.html
---DCH300.py----:
m_dch = 300.0
evgenConfig.description = "Doubly charged higgs ("+str(m_dch)+") in lepton mode."
evgenConfig.process = "DCH -> same sign 2lepton"
evgenConfig.keywords = ["BSM", "chargedHiggs" ,"2lepton"]
evgenConfig.contact = ["Katja Mankinen <katja.hannele.karppinen@cern.ch>"]
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += [
"9900042:m0 = " + str(m_dch), # H++_R mass [GeV]
"9900041:m0 = " + str(m_dch), # H++_L mass [GeV]
"LeftRightSymmmetry:ffbar2HLHL=on", #HL pair production
"LeftRightSymmmetry:ffbar2HRHR=on", #HR pair production
#set the VEV (vacuum expectation value) value
"LeftRightSymmmetry:vL=0.0",
# set all couplings to leptons to 0.1
"LeftRightSymmmetry:coupHee=0.02",
"LeftRightSymmmetry:coupHmue=0.02",
"LeftRightSymmmetry:coupHmumu=0.02",
"LeftRightSymmmetry:coupHtaue=0.0",
"LeftRightSymmmetry:coupHtaumu=0.0",
"LeftRightSymmmetry:coupHtautau=0.0"]
--


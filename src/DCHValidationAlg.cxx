//Updated December 2017 by Simon Arnling Bååth (simon.baath@gmail.com)



// DCHValidation includes
#include "DCHValidationAlg.h"


DCHValidationAlg::DCHValidationAlg( const std::string& name, ISvcLocator* pSvcLocator ) : 
  AthHistogramAlgorithm( name, pSvcLocator ),
  m_McEventCollection(),
  m_DCHmass(300.0)

{

  declareProperty( "DCHmass", m_DCHmass ); //example property declaration

}




DCHValidationAlg::~DCHValidationAlg() {}


StatusCode DCHValidationAlg::initialize() {
  ATH_MSG_INFO ("Initializing DCH Validation for mass = " << m_DCHmass);
  //General histograms. Contains all pairs
  CHECK( book( TH1D( "DecayType", "Decay Type", 8, 0,8 ) ) );
  CHECK( book( TH1D( "FinalStateLeptons", "FinalStateLeptons", 22, 0, 22 ) ) );
  
  CHECK( book( TH1D( "NonDCHTauParents", "NonDCHTauParents", 600, 0, 600) ) );
  CHECK( book( TH1D( "Electrons", "Undecayed/Decayed Electrons", 2, 0,2 ) ) );
  CHECK( book( TH1D( "Muons"    , "Undecayed/Decayed #mu"    , 2, 0,2 ) ) );
  CHECK( book( TH1D( "Tauons"   , "Undecayed/Decayed #tau"   , 2, 0,2 ) ) );
  CHECK( book( TH1D( "LeptonPairTypeP", "Positive Lepton Pair Type", 8, 0,8 ) ) );
  CHECK( book( TH1D( "LeptonPairTypeN", "Negative Lepton Pair Type", 8, 0,8 ) ) );
  CHECK( book( TH1D( "InvariantMassPGlobal", "Positive Lepton Pair Invariant Mass", 1400, 0, 1400 ) ) );
  CHECK( book( TH1D( "InvariantMassNGlobal", "Negative Lepton Pair Invariant Mass", 1400, 0, 1400 ) ) );

  //General histograms with pair of leptons l+l+  or  l-l-
  CHECK( book( TH1D( "InvariantMassP", "Positive Lepton Pair Invariant Mass", 200, m_DCHmass-100, m_DCHmass+100 ) ) );
  CHECK( book( TH1D( "InvariantMassN", "Negative Lepton Pair Invariant Mass", 200, m_DCHmass-100, m_DCHmass+100 ) ) );
  CHECK( book( TH1D( "LeadingPtP"    , "Positive Leading Lepton p_{T}"         , 200, 0, 2500 ) ) );
  CHECK( book( TH1D( "SubLeadingPtP" , "Positive Sub-Leading Lepton p_{T}"     , 200, 0, 1400 ) ) );
  CHECK( book( TH1D( "LeadingEtaP"   , "Positive Leading Lepton #eta"        , 200, -5, 5   ) ) );
  CHECK( book( TH1D( "SubLeadingEtaP", "Positive Sub-Leading Lepton #eta"    , 200, -5, 5   ) ) );
  CHECK( book( TH1D( "LeadingPtN"    , "Negative Leading Lepton p_{T}"         , 200, 0, 2500 ) ) );
  CHECK( book( TH1D( "SubLeadingPtN" , "Negative Sub-Leading Lepton p_{T}"     , 200, 0, 1400 ) ) );
  CHECK( book( TH1D( "LeadingEtaN"   , "Negative Leading Lepton #eta"        , 200, -5, 5   ) ) );
  CHECK( book( TH1D( "SubLeadingEtaN", "Negative Sub-Leading Lepton #eta"    , 200, -5, 5   ) ) );
  
  //Specific histograms for all the lepton combinations
  for (int i = 0; i < 6; i++) {
    std::ostringstream InvariantMassPGlobal; InvariantMassPGlobal << "InvariantMassPGlobal" << m_shortNamePool[i];
    std::ostringstream InvariantMassNGlobal; InvariantMassNGlobal << "InvariantMassNGlobal" << m_shortNamePool[i];
    std::ostringstream InvariantMassP; InvariantMassP << "InvariantMassP" << m_shortNamePool[i];
    std::ostringstream InvariantMassN; InvariantMassN << "InvariantMassN" << m_shortNamePool[i];
    std::ostringstream LeadingPtP    ; LeadingPtP     << "LeadingPtP"     << m_shortNamePool[i];
    std::ostringstream SubLeadingPtP ; SubLeadingPtP  << "SubLeadingPtP"  << m_shortNamePool[i];
    std::ostringstream LeadingEtaP   ; LeadingEtaP    << "LeadingEtaP"    << m_shortNamePool[i];
    std::ostringstream SubLeadingEtaP; SubLeadingEtaP << "SubLeadingEtaP" << m_shortNamePool[i];
    std::ostringstream LeadingPtN    ; LeadingPtN     << "LeadingPtN"     << m_shortNamePool[i];
    std::ostringstream SubLeadingPtN ; SubLeadingPtN  << "SubLeadingPtN"  << m_shortNamePool[i];
    std::ostringstream LeadingEtaN   ; LeadingEtaN    << "LeadingEtaN"    << m_shortNamePool[i];
    std::ostringstream SubLeadingEtaN; SubLeadingEtaN << "SubLeadingEtaN" << m_shortNamePool[i];
    CHECK( book( TH1D( InvariantMassPGlobal.str().c_str(), "Positive Lepton Pair Invariant Mass", 1400, 0, 1400 ) ) );
    CHECK( book( TH1D( InvariantMassNGlobal.str().c_str(), "Negative Lepton Pair Invariant Mass", 1400, 0, 1400 ) ) );
    CHECK( book( TH1D( InvariantMassP.str().c_str(), "Positive Lepton Pair Invariant Mass", 200, m_DCHmass-100, m_DCHmass+100 ) ) );
    CHECK( book( TH1D( InvariantMassN.str().c_str(), "Negative Lepton Pair Invariant Mass", 200, m_DCHmass-100, m_DCHmass+100 ) ) );
    CHECK( book( TH1D( LeadingPtP.str().c_str()    , "Positive Leading Lepton p_{T}"         , 200, 0, 2500 ) ) );
    CHECK( book( TH1D( SubLeadingPtP.str().c_str() , "Positive Sub-Leading Lepton p_{T}"     , 200, 0, 1400 ) ) );
    CHECK( book( TH1D( LeadingEtaP.str().c_str()   , "Positive Leading Lepton #eta"        , 200, -5, 5   ) ) );
    CHECK( book( TH1D( SubLeadingEtaP.str().c_str(), "Positive Sub-Leading Lepton #eta"    , 200, -5, 5   ) ) );
    CHECK( book( TH1D( LeadingPtN.str().c_str()    , "Negative Leading Lepton p_{T}"         , 200, 0, 2500 ) ) );
    CHECK( book( TH1D( SubLeadingPtN.str().c_str() , "Negative Sub-Leading Lepton p_{T}"     , 200, 0, 1400 ) ) );
    CHECK( book( TH1D( LeadingEtaN.str().c_str()   , "Negative Leading Lepton #eta"        , 200, -5, 5   ) ) );
    CHECK( book( TH1D( SubLeadingEtaN.str().c_str(), "Negative Sub-Leading Lepton #eta"    , 200, -5, 5   ) ) );
  }


  return StatusCode::SUCCESS;
}

StatusCode DCHValidationAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}

//*****   Filling histograms and executing the validation script    **************'


StatusCode DCHValidationAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
    

  CHECK( evtStore()->retrieve( m_McEventCollection, "GEN_EVENT" ) );    

  int NgenEvt = 0;
  
  // ****   Loop over all events of the input EvGen sample   ****
  for (const HepMC::GenEvent* genEvt : *m_McEventCollection) {
    NgenEvt++;
    
		int TwoPairType[2] = {1, 1};  
    int nVtx = 0;
    
    //If events with only taus from DCH decay should be recorded, put NonDCHTauFlag to false, else use true
    bool NonDCHTauFlag = true;  
    
    if (!eventHasNonDCHTau(genEvt) or NonDCHTauFlag) {
      
      //****   Loop over all vertices of the event.    ***** 
      for (HepMC::GenEvent::vertex_const_iterator iv = genEvt->vertices_begin(); iv != genEvt->vertices_end(); ++iv) {
      nVtx++;
      ATH_MSG_VERBOSE("     Number of incoming particles: " << (*iv)->particles_in_size() );
      ATH_MSG_VERBOSE("     Number of outgoing particles: " << (*iv)->particles_out_size() );
      
      if ( (*iv)->particles_in_size()!=1 ) {
      	ATH_MSG_VERBOSE("     More than one incoming particle. Continuing..");
      	continue;
      }

      int nPin = 0;
      int nHpp = 0;
      
      //****    Loop over all incoming particles of a vertex     ****
      for (HepMC::GenVertex::particles_in_const_iterator iPIn = (*iv)->particles_in_const_begin();iPIn != (*iv)->particles_in_const_end(); ++iPIn) {
        nPin++;
        
        /*
        if ( std::abs((*iPIn)->pdg_id()) != 9900042 && std::abs((*iPIn)->pdg_id()) != 9900041)  {
        	if (hasTauDaughter(*iPIn)) break;
        	continue;
				}
				*/
        
        //Only continue if the incoming particle of the vertex is a DCH, otherwise continue 
              
        if( std::abs((*iPIn)->pdg_id()) == 9900042 ) {
          ATH_MSG_VERBOSE("     DCH_R is the incoming particle of this vertex");
        }
        else if( std::abs((*iPIn)->pdg_id()) == 9900041 ) {
          ATH_MSG_VERBOSE("     DCH_L is the incoming particle of this vertex");
        }
        
        else continue;
        
        nHpp++;
        if( (*iv)->particles_out_size()==1 ) {
        	ATH_MSG_INFO("     Only one outgoing particle. Continuing..");
        	continue;
        }


				//Initialize loop variables
 
        int nPout = 0;   //Number of particles out of a vertex
        int pairType = 1;  // Lepton combination of decay products of the DCH
        int pairSign = 0;  // Charge of the decay products of the DCH
                
        CLHEP::HepLorentzVector fourMomentum(0,0,0,0);
        HepMC::GenParticle* leadingLepton    = 0;
        HepMC::GenParticle* subLeadingLepton = 0;
        
        ATH_MSG_INFO("     === incoming particle pdg id: " << (*iPIn)->pdg_id() << " ===");
        
        
        // ****    Loop over outgoing particles of a DCH vertex    *****
        
        for (HepMC::GenVertex::particles_out_const_iterator iPOut = (*iv)->particles_out_const_begin();iPOut != (*iv)->particles_out_const_end(); ++iPOut) {
        	
        	nPout++;

          ATH_MSG_INFO("       --------------");
        	ATH_MSG_INFO("          Outgoing particle #" << nPout << " pdg id: " << (*iPOut)->pdg_id() << " barcode: " <<  (*iPOut)->barcode() );
        	ATH_MSG_INFO("          pt: " << (*iPOut)->momentum().perp() << " generated mass: " << (*iPOut)->generated_mass() );
          ATH_MSG_INFO("          status: " << (*iPOut)->status() );
          
          //Check that the DCH decay is correct and only to two particles
          if( (nPout == 3) && ((*iPOut)->pdg_id() != 22) ) {
            ATH_MSG_ERROR("          This algorithm is not meant for these kind of decays. Exiting.");
            return StatusCode::FAILURE;
          }
          else if( nPout > 2 ) continue;
          
          
          
          HepMC::GenParticle* finalStateParticle = findFinalState( (*iPOut) ); //Definig the decay particle after all photon radiation
         
          if (!finalStateParticle) {
            ATH_MSG_ERROR("          No Final state for " << (*iPOut)->barcode() << " was found. Exiting.");
            return StatusCode::FAILURE;
          }
          
          ATH_MSG_INFO("          final particle status: " << finalStateParticle->status() << " barcode: " << finalStateParticle->barcode() << " pt: " << finalStateParticle->momentum().perp() );
        	
        	//If final state particle is a Tau, check wether it is a hadronic tau (will be treated as tau) or a leptonic tau (will be treated as it's decay product) 
        	if (std::abs(finalStateParticle->pdg_id()) == 15) pairType *= tauType(finalStateParticle);
        	else pairType *= std::abs(finalStateParticle->pdg_id());
        	
        	if( std::abs( finalStateParticle->pdg_id() ) == 24 ) pairSign = 0;
        	else {
            
            //-----    Only electrons, muons and hadronic taus from now on      -------
           
           
            //Fill some general histograms about the number of decayed/undecayed leptons///////////////////////
            if( std::abs( finalStateParticle->pdg_id() ) == 11 ) hist("Electrons")->Fill( finalStateParticle->has_decayed() );
            else if( std::abs( finalStateParticle->pdg_id() ) == 13 ) hist("Muons")->Fill( finalStateParticle->has_decayed() );
            else if( std::abs( finalStateParticle->pdg_id() ) == 15 ) hist("Tauons")->Fill( finalStateParticle->has_decayed() );			
                
            pairSign = finalStateParticle->pdg_id()/std::abs( finalStateParticle->pdg_id() );
        		fourMomentum += SVtoLV( finalStateParticle->momentum() );
        		
        		//Defining the leading and subleading lepton
        		if (nPout==1) leadingLepton = finalStateParticle;
        		else if ( finalStateParticle->momentum().perp() > leadingLepton->momentum().perp() ){
        			subLeadingLepton = leadingLepton;
        			leadingLepton = finalStateParticle;
        		}
        		else if ( finalStateParticle->momentum().perp() < leadingLepton->momentum().perp() ){
        			subLeadingLepton = finalStateParticle;
        		}
        		else {
        			ATH_MSG_ERROR("     Something Strange Happened. Exiting.");
        			return StatusCode::FAILURE;
        		}
        	}
        } // End loop over outgoing particles
       
       
        ATH_MSG_INFO("       --------------");
        ATH_MSG_INFO("     Lepton pair type (pdg1*pdg2): " << pairType << " pairSign: " << pairSign );
        
        
        if( pairSign!=0 ) {
        	ATH_MSG_INFO("     Leading lepton pt: " << leadingLepton->momentum().perp() << " subLeading lepton pt: " << subLeadingLepton->momentum().perp() );
          if ( leadingLepton->momentum().perp() < subLeadingLepton->momentum().perp() ){
        	  ATH_MSG_ERROR("     leading pt is smaller than subLeading pt. Exiting.");
        	  return StatusCode::FAILURE;
          }
        }
        
        //****     Filling histograms! :D     ****
        
        
        hist("DecayType")->Fill( getPairType(pairType) );
        
                        
        
        if( pairSign == -1) {
        
          
        	TwoPairType[0] = getPairType(pairType); //Fill TwoPairType for the negative lepton pair
          //General Histograms
          hist("InvariantMassPGlobal" )->Fill( fourMomentum.mag()/1000. );
          hist("InvariantMassP" )->Fill( fourMomentum.mag()/1000. );
        	hist("LeptonPairTypeP")->Fill( getPairType(pairType) );
        	hist("LeadingPtP"     )->Fill( leadingLepton->momentum().perp()/1000 );
        	hist("SubLeadingPtP"  )->Fill( subLeadingLepton->momentum().perp()/1000 );
        	hist("LeadingEtaP"    )->Fill( leadingLepton->momentum().eta() );
        	hist("SubLeadingEtaP" )->Fill( subLeadingLepton->momentum().eta() );
          //Specific Histograms
          hist( getHistogramName(pairType, "InvariantMassPGlobal" ) )->Fill( fourMomentum.mag()/1000. );
          hist( getHistogramName(pairType, "InvariantMassP" ) )->Fill( fourMomentum.mag()/1000. );
          hist( getHistogramName(pairType, "LeadingPtP" )     )->Fill( leadingLepton->momentum().perp()/1000 );
          hist( getHistogramName(pairType, "SubLeadingPtP" )  )->Fill( subLeadingLepton->momentum().perp()/1000 );
          hist( getHistogramName(pairType, "LeadingEtaP" )    )->Fill( leadingLepton->momentum().eta() );
          hist( getHistogramName(pairType, "SubLeadingEtaP" ) )->Fill( subLeadingLepton->momentum().eta() );
          ATH_MSG_VERBOSE("FOUR");
        }
        else if( pairSign == 1) {
        
        	TwoPairType[1] = getPairType(pairType); //Fill TwoPairType for the positive lepton pair
          //General Histogram
          hist("InvariantMassNGlobal") ->Fill( fourMomentum.mag()/1000. );
          hist("InvariantMassN") ->Fill( fourMomentum.mag()/1000. );
        	hist("LeptonPairTypeN")->Fill( getPairType(pairType) );
        	hist("LeadingPtN")     ->Fill( leadingLepton->momentum().perp()/1000 );
        	hist("SubLeadingPtN")  ->Fill( subLeadingLepton->momentum().perp()/1000 );
        	hist("LeadingEtaN")    ->Fill( leadingLepton->momentum().eta() );
        	hist("SubLeadingEtaN") ->Fill( subLeadingLepton->momentum().eta() );
          //Specific Histograms
          hist( getHistogramName(pairType, "InvariantMassNGlobal" ) )->Fill( fourMomentum.mag()/1000. );
          hist( getHistogramName(pairType, "InvariantMassN" ) )->Fill( fourMomentum.mag()/1000. );
          hist( getHistogramName(pairType, "LeadingPtN" )     )->Fill( leadingLepton->momentum().perp()/1000 );
          hist( getHistogramName(pairType, "SubLeadingPtN" )  )->Fill( subLeadingLepton->momentum().perp()/1000 );
          hist( getHistogramName(pairType, "LeadingEtaN" )    )->Fill( leadingLepton->momentum().eta() );
          hist( getHistogramName(pairType, "SubLeadingEtaN" ) )->Fill( subLeadingLepton->momentum().eta() );
          
        }
      } // End loop over incoming particles
             
    } // End loop over vertices
        
    hist("FinalStateLeptons")->Fill(getTwoPairType(TwoPairType));
    ATH_MSG_INFO(" Number of vertices: " << nVtx);
   
   }	//End if statement for checking leaking taus 

    
  } // End loop over GenEvents
  
  ATH_MSG_INFO(" Number of GenEvents: " << NgenEvt);

  return StatusCode::SUCCESS;
}

// getPairType takes the product of the pdg-ID of two leptons and returns an integer for histogram-filling purposes 
// 121 = 11*11 = elel, 
// 225 = 15*15 = tautau, 
// 169 = 13*13 = mumu, 
// 143 = 11*13 = elmu, 
// 195 = 15*13 = taumu, 
// 165 = 16*11 = tauel


int DCHValidationAlg::getPairType(int pdgProd) { 
  if (pdgProd == 121 ) return 1;
  else if (pdgProd == 169 ) return 2;
  else if (pdgProd == 225 ) return 3;
  else if (pdgProd == 143 ) return 4;
  else if (pdgProd == 195 ) return 5;
  else if (pdgProd == 165 ) return 6;
  else if (pdgProd == 576 ) return 7;
  else return 0;
}

//getTwoPairType takes an array [x,y] as input, where x and y are an integer provided by the getPairType function above.
//It returns an integer for histogram purposes that correspond to one of the 21 possible four lepton final states possible för H++H-- pair production. Example [el el,el mu] = [1, 4] = 4, i.e the integer 4 represent the final state 2el/el mu.

int DCHValidationAlg::getTwoPairType(int* TwoPairType) {
	

  if (TwoPairType[0] == 1) { //ee
  
  	if (TwoPairType[1] == 1) return 1; //ee
  	else if (TwoPairType[1] == 2) return 2; //mm
  	else if (TwoPairType[1] == 3) return 3; //tt
  	else if (TwoPairType[1] == 4) return 4; //em
  	else if (TwoPairType[1] == 5) return 5; //tm
  	else if (TwoPairType[1] == 6) return 6; //te
  }
  
  else if (TwoPairType[0] == 2) { //mm
  	
  	if (TwoPairType[1] == 1) return 2; //ee
  	else if (TwoPairType[1] == 2) return 7; //mm
  	else if (TwoPairType[1] == 3) return 8; //tt
  	else if (TwoPairType[1] == 4) return 9; //em
  	else if (TwoPairType[1] == 5) return 10; //tm
  	else if (TwoPairType[1] == 6) return 11; //te
  }
  
  else if (TwoPairType[0] == 3) { //tt
  	
  	if (TwoPairType[1] == 1) return 3; //ee
  	else if (TwoPairType[1] == 2) return 8; //mm
  	
  	else if (TwoPairType[1] == 3) return 12; //tt
  	else if (TwoPairType[1] == 4) return 13; //em
  	else if (TwoPairType[1] == 5) return 14; //tm
  	else if (TwoPairType[1] == 6) return 15; //te
  }
  
  else if (TwoPairType[0] == 4) { //em
  
    if (TwoPairType[1] == 1) return 4; //ee
  	else if (TwoPairType[1] == 2) return 9; //mm
  	else if (TwoPairType[1] == 3) return 13; //tt
   	else if (TwoPairType[1] == 4) return 16; //em
  	else if (TwoPairType[1] == 5) return 17; //tm
  	else if (TwoPairType[1] == 6) return 18; //te
  }   
    
  else if (TwoPairType[0] == 5) { //tm
  
    if (TwoPairType[1] == 1) return 5; //ee
  	else if (TwoPairType[1] == 2) return 10; //mm
  	else if (TwoPairType[1] == 3) return 14; //tt
  	else if (TwoPairType[1] == 4) return 17; //em
  	else if (TwoPairType[1] == 5) return 19; //tm
  	else if (TwoPairType[1] == 6) return 20; //te
  }     

  else if (TwoPairType[0] == 6) { //te
  	
  	if (TwoPairType[1] == 1) return 6; //ee
  	else if (TwoPairType[1] == 2) return 11; //mm
  	else if (TwoPairType[1] == 3) return 15; //tt
  	else if (TwoPairType[1] == 4) return 18; //em
  	else if (TwoPairType[1] == 5) return 20; //tm
  	else if (TwoPairType[1] == 6) return 21; //te
  }   
 
 return 0;
}

const char* DCHValidationAlg::getHistogramName(int pairType, std::string histogramName) {
  return ( histogramName + m_shortNamePool[ getPairType(pairType)-1 ] ).c_str();
}

CLHEP::HepLorentzVector DCHValidationAlg::SVtoLV( const HepMC::FourVector& v ) { 
  return CLHEP::HepLorentzVector( v.x(), v.y(), v.z(), v.t() ); 
}

//findFinalState takes a particle pointer as input, traces it's children and returns the last particle that does not radiate a photon. In the case of electrons and muons this function returns the final state of the particle, in the case of taus the function return a pointer to a undecayed tau that does not radiate anymore photons.

HepMC::GenParticle* DCHValidationAlg::findFinalState(HepMC::GenParticle* p) {
  
  
  if (p->status()==1){
  	
   	return p;}
  HepMC::GenVertex* endVertex = p->end_vertex();
  if (!endVertex) return 0;
  for (HepMC::GenVertex::particles_out_const_iterator iPOut = endVertex->particles_out_const_begin();iPOut != endVertex->particles_out_const_end(); ++iPOut) {
    if ( (*iPOut)->pdg_id() != p->pdg_id() ) continue;
    if ( (*iPOut)->status() == 1 ){
    	
    	 return (*iPOut);}
    else return findFinalState( (*iPOut) );
  }
  // If we get to here, the particle decayed into something else and did not radiate photons. We return it.
  
  return p;
}


//tauType takes a particle pointer as input. If it is a tau lepton, it looks though its children and returns the pdg-ID of the lepton it decays into. If hadronic, the pdg-ID of the tau is returned.  

int DCHValidationAlg::tauType(HepMC::GenParticle* p) {
	if (std::abs(p->pdg_id()) != 15) return std::abs(p->pdg_id());
  
		for (HepMC::GenVertex::particles_out_const_iterator iPOut = (p->end_vertex())->particles_out_const_begin();iPOut != (p->end_vertex())->particles_out_const_end(); ++iPOut){
		
			//ATH_MSG_INFO("The daugther particle is   " << (*iPOut)->pdg_id());		
			if (std::abs((*iPOut)->pdg_id()) == 12) return 11; //If electron neutrino in decay, return electron pdg
			else if (std::abs((*iPOut)->pdg_id()) == 14) return 13; //If muon neutrino in decay, return electron pdg
		}
			
	return 15;		
}


//hasTauDaughter takes a particle pointer as input and checks if it has a tau daughter. If so, it returns true, otherwise false. 

bool DCHValidationAlg::hasTauDaughter(HepMC::GenParticle* p) {
  
		for (HepMC::GenVertex::particles_out_const_iterator iPOut = (p->end_vertex())->particles_out_const_begin();iPOut != (p->end_vertex())->particles_out_const_end(); ++iPOut){
		
					
			if (std::abs((*iPOut)->pdg_id()) == 15 && abs(p->pdg_id()) != 15){
			 		 
			 std::cout <<  "The particle ***  " << p->pdg_id() << "  *** has a tau daughter   " << (*iPOut)->pdg_id() << std::endl;
			 countLeakyTaus++;
			 if (std::abs(p->pdg_id()) <= 600) hist("NonDCHTauParents" )->Fill(std::abs(p->pdg_id()));
			 else hist("NonDCHTauParents" )->Fill(0);
			 std::cout << "Total leaky taus are ---  " << countLeakyTaus << "  --- " << std::endl << std::endl;
			 return true;
			} 
      
		}		
	return false;				
}

//eventHasNonDCHTau takes an event pointer as input and looks through the whole event for non-DCH particles decaying into taus. If found it returns true, else false.

bool DCHValidationAlg::eventHasNonDCHTau(const HepMC::GenEvent* genEvt){

	bool NonDCHTauFlag = false;

	for (HepMC::GenEvent::vertex_const_iterator iv = genEvt->vertices_begin(); iv != genEvt->vertices_end(); ++iv) {
	
		for (HepMC::GenVertex::particles_in_const_iterator iPIn = (*iv)->particles_in_const_begin();iPIn != (*iv)->particles_in_const_end(); ++iPIn) {
		
			if ( std::abs((*iPIn)->pdg_id()) != 9900042 && std::abs((*iPIn)->pdg_id()) != 9900041)  {
        	if (hasTauDaughter(*iPIn)) NonDCHTauFlag = true; 
        	       		
			}
		}
	}
	return NonDCHTauFlag;
}	




#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../DCHValidationAlg.h"

DECLARE_ALGORITHM_FACTORY( DCHValidationAlg )

DECLARE_FACTORY_ENTRIES( DCHValidation ) 
{
  DECLARE_ALGORITHM( DCHValidationAlg );
}

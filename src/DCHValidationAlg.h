#ifndef DCHVALIDATION_DCHVALIDATIONALG_H
#define DCHVALIDATION_DCHVALIDATIONALG_H 1

#include "AthenaBaseComps/AthHistogramAlgorithm.h"

// McEventContainer
#include "GeneratorObjects/McEventCollection.h"

// LorentzVector
#include "CLHEP/Vector/LorentzVector.h"


class DCHValidationAlg: public ::AthHistogramAlgorithm { 
 public: 
  DCHValidationAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~DCHValidationAlg(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private: 

  const McEventCollection*      m_McEventCollection;
  double                        m_DCHmass;
  std::string                   m_shortNamePool[6] = {"e" , "mu", "tau", "emu", "mutau", "etau"};
  int 													countLeakyTaus = 0;

 protected:
  
  int getPairType(int);
  int getTwoPairType(int*);
  int tauType(HepMC::GenParticle*); 
  bool hasTauDaughter(HepMC::GenParticle*);
  bool eventHasNonDCHTau(const HepMC::GenEvent*);
  CLHEP::HepLorentzVector       SVtoLV(const HepMC::FourVector& v );
  HepMC::GenParticle*           findFinalState(HepMC::GenParticle* p);
  const char*                   getHistogramName(int pairType, std::string histogramName);

}; 

#endif 

# echo "cleanup DCHValidation DCHValidation-00-00-00 in /afs/cern.ch/user/m/mmuskinj/MC15evgenProduction/19.2.X.Y-VAL"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.3/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtDCHValidationtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtDCHValidationtempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=DCHValidation -version=DCHValidation-00-00-00 -path=/afs/cern.ch/user/m/mmuskinj/MC15evgenProduction/19.2.X.Y-VAL  $* >${cmtDCHValidationtempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=DCHValidation -version=DCHValidation-00-00-00 -path=/afs/cern.ch/user/m/mmuskinj/MC15evgenProduction/19.2.X.Y-VAL  $* >${cmtDCHValidationtempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtDCHValidationtempfile}
  unset cmtDCHValidationtempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtDCHValidationtempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtDCHValidationtempfile}
unset cmtDCHValidationtempfile
exit $cmtcleanupstatus


# Run arguments file auto-generated on Fri Dec  8 16:28:19 2017 by:
# JobTransform: generate
# Version: $Id: trfExe.py 666344 2015-05-11 20:18:27Z graemes $
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'generate' 

runArgs.randomSeed = 1234
runArgs.generatorJobNumber = 0
runArgs.printEvts = 5
runArgs.runNumber = 301528
runArgs.maxEvents = 500
runArgs.jobConfig = ['DCH300MixedTauHadLep.py']
runArgs.generatorRunMode = 'run'
runArgs.firstEvent = 1
runArgs.ecmEnergy = 13000.0

# Input data

# Output data
runArgs.outputEVNTFile = 'DCH300MixedTauHadLep.EVNT.pool.root'
runArgs.outputEVNTFileType = 'EVNT'

# Extra runargs

# Extra runtime runargs

# Literal runargs snippets
